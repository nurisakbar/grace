<?php
if ( ! function_exists('generatehtml'))
{
    function seo_title($s) {
    $c = array (' ');
    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
    
    $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
    return $s;
}

    function text($type,$name,$class,$placeholder,$required,$values,$tags){
        if(empty($tags))
        {
          $tagtemp="";
        }
        else
        {
                $tagtemp="";
                foreach($tags as $name => $tag)
                {
                    $tagtemp=$tagtemp." $name='$tag' ";
                }
        }
            $requred=$required==0?'':"required='required'";
            
        return "
            <div class='$class'>
            <input type='$type' name='$name' class='form-control' value='$values' placeholder='$placeholder' $required value='$values' $tagtemp>
            </div>";
    }
    
    function combodumy($name,$id)
    {
        return "<select name='$name' id='$id' class='form-control'><option value='0'>Pilih data</option></select>";
    }
    
    function buatcombo($nama,$table,$class,$field,$pk,$id,$kondisi)
    {
        $CI =& get_instance();
        //$CI->load->model('mcrud');
        if($kondisi==null)
        {
          //$CI->db->order_by($pk, "asc"); 
          $data=$CI->db->get($table)->result();  
        }
        else
        {
            //$CI->db->order_by($pk, "asc"); 
            $data=$CI->db->get_where($table,$kondisi)->result();
        }
        echo"<div class='$class'><select name='".$nama."' id='".$id."' class='form-control'>";
        foreach ($data as $r)
        {
            echo" <option value=".$r->$pk.">".strtoupper($r->$field)."</option>";
        }
            echo"</select></div>";
    }
    
    
    function chek_login(){
        $CI =& get_instance();
        if($CI->session->userdata('username')==''){
            redirect('adm/auth/login');
        }
    }
    
    function submit($value,$class)
    {
        return "<button type='submit' class='$class' name='submit'>$value</button>";
    }
    
       function rp($x)
       {
           return number_format($x,0,",",".");
       }
       
       function waktu()
       {
           date_default_timezone_set('Asia/Jakarta');
           return date("Y-m-d H:i:s");
       }
              
       function tgl_indo($tgl)
       {
            return substr($tgl, 8, 2).' '.getbln(substr($tgl, 5,2)).' '.substr($tgl, 0, 4);
       }
    
    function tgl_indojam($tgl,$pemisah)
    {
        return substr($tgl, 8, 2).' '.getbln(substr($tgl, 5,2)).' '.substr($tgl, 0, 4).' '.$pemisah.' '.  substr($tgl, 11,8);
    }
    
    
    function getbln($bln)
    {
        switch ($bln) 
        {
            
            case 1:
                return "Januari";
            break;
        
            case 2:
                return "Februari";
            break;
        
            case 3:
                return "Maret";
            break;
        
            case 4:
                return "April";
            break;
        
            case 5:
                return "Mei";
            break;
        
            case 6:
                return "Juni";
            break;
        
            case 7:
                return "Juli";
            break;
        
            case 8:
                return "Agustus";
            break;
        
            case 9:
                return "September";
            break;
        
             case 10:
                return "Oktober";
            break;
        
            case 11:
                return "November";
            break;
        
            case 12:
                return "Desember";
            break;
        }
        
    }
    
    
    //Fungsi untuk meng-upload gambar
function UploadImage($img_name,$foldername){
//header("Content-type: image/jpeg");

//direktori gambar
$vdir_upload = "assets/$foldername/";
$vfile_upload = $vdir_upload . $img_name;

//Simpan gambar dalam ukuran sebenarnya
move_uploaded_file($_FILES["userfile"]["tmp_name"], $vfile_upload);

//identitas file asli
$im_src = imagecreatefromjpeg($vfile_upload);
$src_width = imageSX($im_src);
$src_height = imageSY($im_src);

//Simpan dalam versi small 110 pixel
//set ukuran gambar hasil perubahan
$dst_width = 110;
$dst_height = ($dst_width/$src_width)*$src_height;

//proses perubahan ukuran
$im = imagecreatetruecolor($dst_width,$dst_height);
imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

//Simpan gambar
imagejpeg($im,$vdir_upload . "small_" . $img_name);

//Simpan dalam versi medium 320 pixel
//set ukuran gambar hasil perubahan
$dst_width = 320;
$dst_height = ($dst_width/$src_width)*$src_height;

//proses perubahan ukuran
$im = imagecreatetruecolor($dst_width,$dst_height);
imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

//Simpan gambar
imagejpeg($im,$vdir_upload . "medium_" . $img_name);

imagedestroy($im_src);
imagedestroy($im);

	//print("<SCRIPT LANGUAGE=\"Javascript\">");
	//print("window.location='index.php?load=banner'"); 
	//print("</SCRIPT>");
}
    
    function namaHari($hari){
				if($hari == "Sunday"){
					$hari = "Minggu";
						return $hari;
				}elseif($hari == "Monday"){
					$hari = "Senin";
						return $hari;
				}elseif($hari == "Thursday"){
					$hari = "Selasa";
						return $hari;
				}elseif($hari == "Wednesday"){
					$hari = "Rabu";
						return $hari;
				}elseif($hari == "Monday"){
					$hari = "Kamis";
						return $hari;
				}elseif($hari == "Friday"){
					$hari = "jumat";
						return $hari;
				}elseif($hari == "Saturday"){
					$hari = "Sabtu";
						return $hari;
				}
			} 
}
?>
