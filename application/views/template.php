<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Grace Technic - your best choice to provide efficient solution</title>

<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style2.css">
<link href="<?php echo base_url();?>assets/css/begog.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="d-head">
  <div id="logo"><img src="<?php echo base_url();?>assets/images/logo.png"></div>
  <div id="head-kanan">
      <span class="besar">Grace Technic </span>
      <span class="slogan">
      <br><br>
      your best choice to provide efficient solution</span>  <br>
  </div>
  <div id="banner-dalam">
  <div id="bahasa">
      <?php
      echo anchor('lang/index/id','Indonesia');
      ?> | 
      <?php
      echo anchor('lang/index/eng','English');
      ?>
</div>
</div>
  <div id="clr"></div>
</div>

<div id="menu">
  <div id="d-menu">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <nav>
	<ul>
	
        
        <?php
        $menu=$this->db->query('select * from menu order by id_menu');
        foreach ($menu->result() as $m){
                // kondisi bahasa di main menu single
                $menu1=$this->session->userdata('lang')=='id'?$m->title:$m->title_eng;
                // end
                // 
            // chek ada submenu atau tidak
            $submenu=$this->db->get_where('submenu',array('id_menu'=>$m->id_menu));
            if($submenu->num_rows()<1){
                echo "<li>".anchor($m->url,$menu1)."</li>";
            }else{
                // chek subsubmenu
                echo "<li>".anchor($m->url,$menu1)."<ul>";
                foreach ($submenu->result() as $sb){
                    // chek sub submenu
                    $subsub=$this->db->get_where('subsubmenu',array('id_submenu'=>$sb->id_submenu));
                    // chek bahasa sub menu
                     $menu2=$this->session->userdata('lang')=='id'?$sb->title:$sb->title_eng;
                     // end
                     

                    if($subsub->num_rows()>0){
                        echo "<li>".anchor($sb->url,$menu2)."<ul>";
                        foreach ($subsub->result() as $ssb){
                             $menu3=$this->session->userdata('lang')=='id'?$ssb->judul:$ssb->judul_eng;
                            echo "<li>".anchor($ssb->link,$menu3)."</li>";
                        }   
                        echo "</ul></li>";
                    }else{
                        echo "<li>".anchor($sb->url,$menu2)."</li>";
                    }
                    
                }
                echo "</ul></li>";
            }
        }
        ?>
	</ul>
</nav>
        
</td>
    <td><div id="menukanan">
            <?php
            echo form_open('news/search/');
            ?>
            <input type="text" class="cari" name="keyword" placeholder="Cari...">
        </form> 
    </div></td>
  </tr>
</table>
</div>
</div>
<?php include "assets/banner.php"; ?>
    
 <?php echo $contents; ?> 
<div id="kareer">
<div id="d-kareer">
    <div id="logo-karir"><img src="<?php echo base_url();?>assets/images/career.png" width="100" height="100" class="logocarir"></div>
    <h2>Your Career Start Here</h2>
      <strong>Lorem Ipsum</strong> is simply dummy text of the printing and   typesetting industry. Lorem Ipsum has been the industry's standard dummy   text ever since the 1500s, when an unknown printer took a galley of   type and scrambled it to make a type specimen book.</div>
</div>
<div id="foot">
  <div id="d-foot">
      <div id="foot-1"><h2>
        <?php
        $menu=$this->db->get_where('menu',array('id_menu'=>1))->row_array();
        echo $this->session->userdata('lang')=='id'?$menu['title']:$menu['title_eng'];
        ?></h2>
        <p>
      <?php
      $tentang=$this->db->get_where('submenu',array('id_menu'=>1));
      foreach ($tentang->result() as $t){
          echo anchor($t->url,$this->session->userdata('lang')=='id'?$t->title:$t->title_eng).'<br>';
      }
      ?>
  </p>
    </div>
    
    <div id="foot-1">
     <h2>
        <?php
        $menu=$this->db->get_where('menu',array('id_menu'=>3))->row_array();
        echo $this->session->userdata('lang')=='id'?$menu['title']:$menu['title_eng'];
        ?></h2>
           <?php
      $tentang=$this->db->get_where('submenu',array('id_menu'=>3));
      foreach ($tentang->result() as $t){
          echo anchor($t->url,$this->session->userdata('lang')=='id'?$t->title:$t->title_eng).'<br>';
      }
      ?>
    </div>
    
    <div id="foot-1">
     <h2>
        <?php
        $menu=$this->db->get_where('menu',array('id_menu'=>5))->row_array();
        echo $this->session->userdata('lang')=='id'?$menu['title']:$menu['title_eng'];
        ?></h2>
      <p><?php
        $menu=$this->db->get_where('menu',array('id_menu'=>5))->row_array();
        echo $this->session->userdata('lang')=='id'?$menu['title']:$menu['title_eng'];
        ?></p>
    </div>
    
    <div id="foot-1">
     <h2>
        <?php
        $menu=$this->db->get_where('menu',array('id_menu'=>6))->row_array();
        echo $this->session->userdata('lang')=='id'?$menu['title']:$menu['title_eng'];
        ?></h2>
      <p><?php echo anchor('kontak','Alamat Kami')?></p>
    </div>
    <div id="clr"></div>
  </div>
  <div id="d-foot-coppy">
  <?php
  $profile=$this->db->get_where('konfigurasi',array('id'=>1))->row_array();
  ?>
  Copyright &copy; 2014 - Grace Technic<br>
  <?php echo $profile['alamat']?><br>
  email : <?php echo $profile['email']?></div>
</div>
</body>
</html>
