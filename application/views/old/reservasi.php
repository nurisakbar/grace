﻿<div class="span8" id="divMain">
    <h1>Reservasi Online</h1>
    <h4 style="color:#FF6633;"><?php echo $this->session->flashdata('pesan');?></h4>
    <hr>
			<!--Start Contact form -->	
                        <?php
                        echo form_open('reservasi',array('class'=>'form-horizontal'));
                        ?>
                        
  
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">Nama </label>
            <div class="span6 controls">
                <input type="text" name="nama" id="inputEmail" class="input-xlarge" required="required">
            </div>
            </div>
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">No. Telp </label>
            <div class="span4 controls">
                <input type="text" name="telpon" id="inputEmail" required="required">
            </div>
            </div>
               
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">Email </label>
            <div class="span6 controls">
                <input type="email" name="email" id="inputEmail"  class="input-xxlarge" required="required">
            </div>
            </div>
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">Jenis </label>
            <div class="span4 controls">
            <select name="jenis">
              <option value="Ruang Standart<">Ruang Standart</option>
              <option value="Gedung Penerima  I">Gedung Penerima  I</option>
              <option value="Gedung Penerima II">Gedung Penerima II</option>
              <option value="Halaman Parkir">Halaman Parkir</option>
            </select>
            </div>
            </div>
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">Tanggal Awal </label>
            <div class="span4 controls">
                <input type="text" name="tanggal_awal" id="awal" required="required">
            </div>
            </div>
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">Tanggal Akhir </label>
            <div class="span4 controls">
                <input type="text" name="tanggal_akhir" id="akhir" required="required">
            </div>
            </div>
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail">Keterangan </label>
            <div class="span8 controls">
            <textarea name="keterangan" rows="3" class="input-xxlarge"></textarea>
            </div>
            </div>
            
            <div class="control-group">
            <label class="span2 control-label" for="inputEmail"></label>
            <div class="span4 controls">
                <button type="submit" name="submit" class="btn">Kirim</button>
            </div>
            </div>
            </form>	 
<!--End Contact form -->											 
</div>
				
<!--Edit Sidebar Content here-->
<div class="span4 sidebar">

    <div class="sidebox">
        <h3 class="sidebox-title">Berita Terbaru</h3>
        <?php foreach ($rberita->result() as $r)  { ?>
        <?php echo anchor('berita/baca/'.$r->id_berita.'/'.$r->judul_seo,  substr($r->judul, 0,40));?>
        <p><div class="span5"><img src="<?php echo base_url();?>assets/berita/<?php echo $r->foto?>" class="img-polaroid" style="margin:10px 5px 5px -15px;" alt="Profile DOME BAlikpapan"></div> 
        
            <?php 
                           $isi_berita = strip_tags($r->deskripsi); // membuat paragraf pada isi berita dan mengabaikan tag html
                    $isi = substr($isi_berita,0,90); // ambil sebanyak 100 karakter
                    $isi = substr($isi_berita,0,strrpos($isi," ")); // potong per spasi kalimat
                    echo $isi;?>
        <hr />
		<?php } ?>   
          
    </div>
    
</div>
<!--End Sidebar Content here-->

<link type="text/css" href="<?php echo base_url();?>assets/kalender/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/kalender/jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/ui.datepicker.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/i18n/ui.datepicker-id.js"></script>
<script type="text/javascript"> 
var $tglawal = jQuery.noConflict();
  $tglawal(document).ready(function(){
    $tglawal("#awal").datepicker({
      dateFormat  : "yy-mm-dd",        
      changeMonth : true				
    });
  });
  var $tglalhir = jQuery.noConflict();
  $tglalhir(document).ready(function(){
    $tglalhir("#akhir").datepicker({
      dateFormat  : "yy-mm-dd",        
      changeMonth : true				
    });
  });
</script>
