﻿<!--Edit Main Content Area here-->
    <div class="span8" id="divMain">
        <br> <h4>Jadwal Fix Pengunaan Gedung</h4>
    <table class="table table-bordered">
            <tr><th>No</th><th>Tanggal</th><th>Fasilitas</th><th>Acara</th><th>Penyewa</th></tr> 
            <?php
            $no=1;
         
            foreach ($jadwal as $j){
                echo "<tr>"
                . "<td>$no</td>"
                . "<td>".  tgl_indo($j->tanggal_awal)."</td>"
                . "<td>".  strtoupper($j->jenis)."</td>"
                . "<td>".  strtoupper($j->keterangan)."</td>"
                . "<td>".  strtoupper($j->nama)."</td>"
                . "</tr>";
                $no++;
            }
            ?>
        </table>
                             
        
    </div>
<!--End Main Content Area here-->
<!--Edit Sidebar Content here-->
<div class="span4 sidebar">

    <div class="sidebox">
        <h3 class="sidebox-title">Berita Terbaru</h3>
        <?php foreach ($rberita->result() as $r)  { ?>
        <?php echo anchor('berita/baca/'.$r->id_berita.'/'.$r->judul_seo,  substr($r->judul, 0,40));?>
        <p><div class="span5"><img src="<?php echo base_url();?>assets/berita/<?php echo $r->foto?>" class="img-polaroid" style="margin:10px 5px 5px -15px;" alt="Profile DOME BAlikpapan"></div> 
        
            <?php 
                           $isi_berita = strip_tags($r->deskripsi); // membuat paragraf pada isi berita dan mengabaikan tag html
                    $isi = substr($isi_berita,0,90); // ambil sebanyak 100 karakter
                    $isi = substr($isi_berita,0,strrpos($isi," ")); // potong per spasi kalimat
                    echo $isi;?>
        <hr />
		<?php } ?>   
          
    </div>
    
</div>
<!--End Sidebar Content here-->