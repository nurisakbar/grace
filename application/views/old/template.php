﻿<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <?php
    $konfigurasi=$this->db->get_where('konfigurasi',array('id'=>1))->row_array();
    ?>
    <title><?php echo $judul;?> :: <?php echo $konfigurasi['title'];?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $konfigurasi['deskripsi'];?>">
    <meta name="author" content="Html5TemplatesDreamweaver.com">

    <link href="<?php echo base_url();?>assets/scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->
    <link href="<?php echo base_url();?>assets/scripts/icons/general/stylesheets/general_foundicons.css" media="screen" rel="stylesheet" type="text/css" />  
    <link href="<?php echo base_url();?>assets/scripts/icons/social/stylesheets/social_foundicons.css" media="screen" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/scripts/fontawesome/css/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <link href="<?php echo base_url();?>assets/scripts/carousel/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/scripts/camera/css/camera.css" rel="stylesheet" type="text/css" />

    <link href="http://fonts.googleapis.com/css?family=Syncopate" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Pontano+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url();?>assets/styles/custom.css" rel="stylesheet" type="text/css" />
</head>
<body id="pageBody">

<div id="divBoxed" class="container">

    <div class="transparent-bg" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: -1;zoom: 1;"></div>

    <div class="divPanel notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                       <img src="<?php echo base_url();?>assets/images/logo2.png"> 
                    </div>

                    <div id="divMenuRight" class="pull-right">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                            NAVIGATION <span class="icon-chevron-down icon-white"></span>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                            <?php if($aktif =='beranda'){ echo "<li class='active'>"; }else{ echo "<li>";} echo anchor('dome','Beranda');?></li>
                            <?php if($aktif =='profil'){ echo "<li class='active'>"; }else{ echo "<li>";} echo anchor('profil','Profil');?></li>
                            <?php if($aktif =='fasilitas'){ echo "<li class='active'>"; }else{ echo "<li>";} echo anchor('fasilitas','Fasilitas');?></li>
                            <?php if($aktif =='tarif'){ echo "<li class='active'>"; }else{ echo "<li>";} echo anchor('tarif','Tarif');?></li>
                            <?php if($aktif =='reservasi'){ echo "<li class='dropdown active'>"; }else{ echo "<li class='dropdown'>"; } ?><a href="#" class="dropdown-toggle">Reservasi <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><?php echo anchor('reservasi','Reservasi Gedung');?></li>
                                    <li><?php echo anchor('jadwal','Jadwal Penggunaan Gedung');?></li>
                                </ul>
                            </li>
							<?php if($aktif =='update'){ echo "<li class='dropdown active'>"; }else{ echo "<li class='dropdown'>"; } ?><a href="#" class="dropdown-toggle">Update <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><?php echo anchor('berita','Berita');?></li>
                                    <li><?php echo anchor('gallery','Gallery');?></li>
                                </ul>
                            </li>
						    <?php if($aktif =='kontak'){ echo "<li class='active'>"; }else{ echo "<li>";} echo anchor('kontak','Kontak');?></li>
                            </ul>
                            </div>
                    </div>
                    </div>

                </div>
            </div>
            
                </div>

    <div class="contentArea">

        <div class="divPanel notop page-content">

           <hr>
            
            
<div class="row-fluid"><?php echo $contents;?></div>
            
	
            <div id="footerInnerSeparator"></div>
        </div>
    </div>

    <div id="footerOuterSeparator"></div>

    <div id="divFooter" class="footerArea">

        <div class="divPanel">

            <div class="row-fluid">
                <div class="span4" id="footerArea1">
                
                      <?php
                $pro=$this->db->get_where('content',array('id_content'=>1))->row_array();
               echo "<h3>".$pro['judul']."</h3><p>".substr($pro['isi'],0,299)."</p>";
                ?>  

                </div>
                <div class="span4" id="footerArea2">

                    <h3>Berita terbaru</h3> 
                    <?php
                    $berita=$this->db->query('select * from berita order by created DESC limit 4');
                    foreach ($berita->result() as $br){
                        echo "<p>".anchor('berita/baca/'.$br->id_berita.'/'.$br->judul_seo,$br->judul,$br->judul)."</p><span style='text-transform:none;'>".  tgl_indo($br->created)."</span>";
                    }
                    ?>
                    
                    <p>
                        <?php
                        echo anchor('berita/','Lihat Berita Lebih Banyak');
                        ?>
                       
                    </p>

                </div>
               
                <div class="span4" id="footerArea4">

                    <h3>Get in Touch</h3>  
                                                               
                    <ul id="contact-info">
                        <?php
                    $co=$this->db->get_where('konfigurasi',array('id_skpd'=>20))->row_array();
                    ?>
                    <li>                                    
                        <i class="general foundicon-phone icon"></i>
                        <span class="field">Phone:</span>
                        <br />
                       <?php echo $co['telp']?>                                                                  
                    </li>
                    <li>
                        <i class="general foundicon-mail icon"></i>
                        <span class="field">Email:</span>
                        <br />
                        <a href="mailto:info@yourdomain.com" title="Email"><?php echo $co['email']?></a>
                    </li>
                    <li>
                        <i class="general foundicon-home icon" style="margin-bottom:50px"></i>
                        <span class="field">Alamat:</span>
                        <br />
                        <?php echo $co['alamat']?>
                    </li>
                    </ul>

                </div>
            </div>

            <br /><br />
            <div class="row-fluid">
                <div class="span12">
                    <p class="copyright">
                        Copyright © 2014 Dinas Pekerjaan Umum - Pemerintah Kota Balikpapan.
                    </p>
                    <p class="social_bookmarks">
            <a href="#"><i class="social foundicon-facebook"></i> Facebook</a>
			<a href=""><i class="social foundicon-twitter"></i> Twitter</a>
			<a href="#"><i class="social foundicon-rss"></i> Rss</a>
                    </p>
                </div>
            </div>

        </div>
    </div>
</div>
<br /><br /><br />

<script src="<?php echo base_url();?>assets/scripts/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/scripts/default.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>assets/scripts/carousel/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script>
<script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 2, items: {width: 320,visible: {min: 2, max: 6}} });</script>

<script src="<?php echo base_url();?>assets/scripts/camera/scripts/camera.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/scripts/easing/jquery.easing.1.3.js" type="text/javascript"></script>

<script type="text/javascript">function startCamera() {$('#camera_wrap').camera({ fx: 'scrollLeft', time: 2000, loader: 'none', playPause: false, navigation: true, height: '35%', pagination: true });}$(function(){startCamera()});</script>


</body>
</html>