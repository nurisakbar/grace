<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Administrator</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login Admin</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open('adm/auth/login');?>
                    <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="user" autofocus id="user">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="pass" type="password" value="" id="pass">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                   
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <label><button type="submit" name="submit" class="btn btn-primary">Submit Button</button></label>
                            </fieldset>
                        </form>
                  </div>
                </div>
            </div>
        </div>
    </div>


</body>

</html>
