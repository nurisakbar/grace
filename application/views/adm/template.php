
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>BackOffice :: Admin</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
    
      <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Administrator / <?php echo $this->session->userdata['namaLengkap']; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            <li>
                <?php
                echo anchor(base_url().'adm/publik/dashboard','<i class="fa fa-dashboard fa-fw"></i> Dashboard');
                ?>
               
            </li>
                    <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!--
                        <?php
                        echo "<li>".anchor('users/profile/'.$this->session->userdata('id'),'<i class="fa fa-user fa-fw"></i> User Profile')."</li>";
                        ?>
                        
                        </li>
                        <?php if($this->session->userdata('level')=='Super Admin' or $this->session->userdata('level')=='Administrator')
						{ ?>
                        <?php
                        echo "<li>".anchor('publik/konfig/','<i class="fa fa-gear fa-fw"></i> Web Config')."</li>";
                        ?>
                        
                        <?php
                        echo "<li>".anchor('users/','<i class="fa fa-user fa-fw"></i> Add User')."</li>";?>
                        <?php } ?>
                        <li class="divider"></li> -->
                        <?php
                        echo "<li>".anchor('adm/publik/konfig/','<i class="fa fa-gear fa-fw"></i> Web Config')."</li>";
                        echo "<li>".anchor('adm/auth/logout','<i class="fa fa-sign-out fa-fw"></i> Logout')."</li>";
                        ?>
                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                
            


						<li>
                                                     <?php
                            $menu_name=array('Banner','Profile','Berita','Content','Event','Gallery','Service',"menu",'Submenu',"Subsubmenu");
                            $menu_link=array('banner','profile','berita','content','event','gallery','service','menu','submenu','subsubmenu');
                            for($me=0;$me<=9;$me++){
                                echo "<li>".anchor('adm/'.$menu_link[$me],'<i class="glyphicon glyphicon-th-large"></i> Module '.$menu_name[$me])."</li>";
                            }
                            ?>
                           
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
 <?php echo $contents; ?> 
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

  

</body>

</html>
