<div class="row">
<div class="col-lg-12"><h3 class="page-header">Dashboard</h3></div>
</div>

<div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Statistik Website
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Jumlah Berita/Artikel
                                    <span class="pull-right text-muted small"><em>30 Artikel</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-twitter fa-fw"></i> Jumlah Pegawai
                                    <span class="pull-right text-muted small"><em>45 Pegawai</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-envelope fa-fw"></i> Jumlah User
                                    <span class="pull-right text-muted small"><em>2 user</em>
                                    </span>
                                </a>
                                
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                   