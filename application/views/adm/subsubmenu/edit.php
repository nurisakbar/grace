<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">EDIT SUBMENU</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
echo form_open_multipart('adm/subsubmenu/edit',array('class'=>'form-horizontal'));
?>
<input type="hidden" name="id" value="<?php echo $r['id_subsubmenu']?>">
  <!-- input -->


  
  <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Ind]</label>
    <div class="col-sm-4">
      <input name="judul" value="<?php echo $r['judul']?>" class="form-control"  />
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Eng] </label>
    <div class="col-sm-4">
      <input name="judul_eng" value="<?php echo $r['judul_eng']?>" class="form-control"  />
    </div>
  </div>
        <div class="form-group">
    <label class="col-sm-2 control-label">Maimenu </label>
    <div class="col-sm-4">
        <select name="menu" class="form-control">
            <?php
            $menu=$this->db->get('submenu');
            foreach ($menu->result() as $m){
                echo "<option value='$m->id_submenu' ";
                echo $m->id_submenu==$r['id_submenu']?"selected":'';
                echo" > ".$m->title."</option>";
            }
            ?>
        </select>
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Link</label>
    <div class="col-sm-4">
        <input name="lingnya" value="<?php echo $r['link']?>" class="form-control" />
    </div>
  </div>

  
 <!-- BUTTON -->   
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
    <div class="col-sm-2">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>