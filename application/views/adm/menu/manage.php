<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">TAMBAH MENU</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
echo form_open_multipart('adm/menu/baru',array('class'=>'form-horizontal'));
?>

  <!-- input -->

  <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Ind] </label>
    <div class="col-sm-4">
      <input name="judul" class="form-control" />
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Eng] </label>
    <div class="col-sm-4">
      <input name="judul_eng" class="form-control" />
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-2 control-label">Link</label>
    <div class="col-sm-4">
      <input name="lingnya" class="form-control" />
    </div>
  </div>

 <!-- BUTTON -->   
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
    <div class="col-sm-2">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>