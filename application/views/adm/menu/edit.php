<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">EDIT MENU</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
echo form_open_multipart('adm/menu/edit',array('class'=>'form-horizontal'));
?>
<input type="hidden" name="id" value="<?php echo $r['id_menu']?>">
  <!-- input -->


  
  <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Ind]</label>
    <div class="col-sm-4">
      <input name="judul" value="<?php echo $r['title']?>" class="form-control"  />
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Eng] </label>
    <div class="col-sm-4">
      <input name="judul_eng" value="<?php echo $r['title_eng']?>" class="form-control"  />
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Link Banner </label>
    <div class="col-sm-4">
        <input name="lingnya" value="<?php echo $r['url']?>" class="form-control" />
    </div>
  </div>

  
 <!-- BUTTON -->   
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
    <div class="col-sm-2">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>