<script type="text/javascript" src="<?php echo base_url()?>assets/mce/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
function ajaxfilemanager(field_name, url, type, win) {
   var ajaxfilemanagerurl = "<?php echo base_url()?>assets/mce/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
   switch (type) {
    case "image":
     break;
    case "media":
     break;
    case "flash": 
     break;
    case "file":
     break;
    default:
     return false;
   }
            tinyMCE.activeEditor.windowManager.open({
                url: "<?php echo base_url()?>assets/mce/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
             /*            return false;   
   var fileBrowserWindow = new Array();
   fileBrowserWindow["file"] = ajaxfilemanagerurl;
   fileBrowserWindow["title"] = "Ajax File Manager";
   fileBrowserWindow["width"] = "782";
   fileBrowserWindow["height"] = "440";
   fileBrowserWindow["close_previous"] = "no";
   tinyMCE.openWindow(fileBrowserWindow, {
     window : win,
     input : field_name,
     resizable : "yes",
     inline : "yes",
     editor_id : tinyMCE.getWindowArg("editor_id")
   });
   
   return false;*/
  }
</script>

<script type="text/javascript">
 tinyMCE.init({
  
  // General options
  mode : "textareas",
  elements : "ajaxfilemanager",
  file_browser_callback : 'ajaxfilemanager',
  theme : "advanced",
  plugins : "safari,pagebreak,style,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

  // Theme options
  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
 theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
 theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
 theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
 
  theme_advanced_toolbar_location : "top",
  theme_advanced_toolbar_align : "left",
  theme_advanced_statusbar_location : "bottom",
  theme_advanced_resizing : true,
  relative_urls : false,
  remove_script_host : false,
  // Example content CSS (should be your site CSS)
  content_css : "css/content.css",

  // Drop lists for link/image/media/template dialogs
  

  // Replace values for the template plugin
  template_replace_values : {
   username : "Some User",
   staffid : "991234"
  }
 });
</script>





<div class="row">
    <div class="col-lg-12"><h3 class="page-header">Tambah Berita</h3></div>
</div>
<?php
echo form_open_multipart('adm/berita/edit',array('class'=>'form-horizontal'));
?>
  <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Ind]</label>
    <input name="id" type="hidden" id="id" value="<?php echo $r['id_berita']; ?>" />
<div class="col-sm-6">
      <input name="judul" class="form-control" value="<?php  echo $r['judul']; ?>" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Judul [Eng]</label>
<div class="col-sm-6">
      <input name="judul_eng" class="form-control"  value="<?php  echo $r['judul_eng']; ?>"  />
    </div>
  </div>
  <div class="form-group">
    <label  class="col-sm-2 control-label">Deskripsi</label>
    <div class="col-sm-10">
<textarea name="isi" rows="10" cols="140" style="width: 80%" id="isis"><?php echo $r['isi'];  ?></textarea>
    </div>
  </div>


  <div class="form-group">
    <label  class="col-sm-2 control-label">Isi Berita</label>
    <div class="col-sm-10">
<textarea name="isi_eng" rows="10" cols="130" style="width: 80%" id="isi"><?php echo $r['isi_eng'];  ?></textarea>
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-2 control-label">Foto Berita</label>
    <div class="col-sm-4">
      <input name="userfile"  type="file" size="35" id="userfile" />
    </div>
  </div>
  
   
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>
