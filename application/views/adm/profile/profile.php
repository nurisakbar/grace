<style type="text/css" title="currentStyle">		
		@import "css/demo_table_jui.css";
		@import "css/jquery-ui-1.8.4.custom.css";
	</style>
	<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			oTable = $('#example').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
		} );
	</script>
<script>
function godelete(id)
            { ans = confirm("Data akan di HAPUS?\n");
              if(ans)
              { document.mainform.action = "profile/delete/" + id + " ";
                document.mainform.submit();
              }
            }
</script>
<form method=post name=mainform>
<script>document.mainform.name.focus();</script>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Profile</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="box-header">
    <?php
    echo anchor('adm/profile/baru','<i class="fa fa-plus-circle"></i> Tambah Profile',array('class'=>'btn btn-default'));
    ?>
   </div>
<br />
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Lebar Header 1000 / Tinggi Menyesuaikan</div>
                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
<table width="100%" border="0" cellpadding="2" class="table table-striped table-bordered table-hover" id="dataTables-example">
  <thead>
  <tr>
    <th width="30" align="center" class="isitable">No</th>
    <th width="493" align="center" class="isitable">Tahun</th>
    <th width="408" align="center" class="isitable">Content</th>
    <th width="408" align="center" class="isitable">Gambar</th>
    <th width="128" align="center" class="isitable">Hapus</th> 
  </tr>
  </thead>
<?php
$nomer = 1;
foreach ($record->result() as $ban)
{
?>
  <tr>
    <td width="30" class="isitable"><?php echo $nomer; ?></td>
    <td width="493" class="isitable"><?php echo $ban->tahun; ?></td>
    <td width="493" class="isitable"><?php echo substr($ban->content,0,200) ?></td>
        <td width="493" class="isitable"><img src="<?php echo base_url()?>assets/profil/<?php echo $ban->gambar; ?>" width="100"></td>
    <td width="128" class="isitable"><div align="center">
	<?php echo anchor('adm/profile/edit/'.$ban->id_profile,'<i class="
glyphicon glyphicon-edit"></i>')?>
        <?php echo "<a href='javascript:godelete($ban->id_profile)'>"; ?>
	<i class="glyphicon glyphicon-trash"></i></a></div></td>
  </tr>
  <?php
  $nomer = $nomer + 1;
  }
  ?>
</table>
</div>
<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>