<style type="text/css" title="currentStyle">		
		@import "css/demo_table_jui.css";
		@import "css/jquery-ui-1.8.4.custom.css";
	</style>
	<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			oTable = $('#example').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
		} );
	</script>
<script>
function godelete(id)
            { ans = confirm("Data akan di HAPUS?\n");
              if(ans)
              { document.mainform.action = "submenu/delete/" + id + " ";
                document.mainform.submit();
              }
            }
</script>
<form method=post name=mainform>
<script>document.mainform.name.focus();</script>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Submenu</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="box-header">
    <?php
    echo anchor('adm/submenu/baru','<i class="fa fa-plus-circle"></i> Tambah Menu',array('class'=>'btn btn-default'));
    ?>
   </div>
<br />
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Lebar Header 1000 / Tinggi Menyesuaikan</div>
                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
<table width="100%" border="0" cellpadding="2" class="table table-striped table-bordered table-hover" id="dataTables-example">
  <thead>
  <tr>
    <th width="30" align="center" class="isitable">No</th>
    <th width="493" align="center" class="isitable">Menu</th>
    <th width="493" align="center" class="isitable">Menu [Eng] </th>
    <th width="408" align="center" class="isitable">Link</th>
    <th colspan="2" width="128" align="center" class="isitable">Hapus</th> 
  </tr>
  </thead>
<?php
$nomer = 1;
foreach ($record->result() as $ban)
{
?>
  <tr>
    <td width="30" class="isitable"><?php echo $nomer; ?></td>
    <td width="493" class="isitable"><?php echo $ban->title; ?></td>
        <td width="493" class="isitable"><?php echo $ban->title_eng; ?></td>
    <td width="493" class="isitable"><?php echo $ban->url; ?></td>
    <td width="128" class="isitable"><div align="center">
	<?php echo anchor('adm/submenu/edit/'.$ban->id_submenu,'<i class="
glyphicon glyphicon-edit"></i>')?>
        <?php echo "<a href='javascript:godelete($ban->id_submenu)'>"; ?>
	<i class="glyphicon glyphicon-trash"></i></a></div></td>
  </tr>
  <?php
  $nomer = $nomer + 1;
  }
  ?>
</table>
</div>
<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>