  <script>
function godelete(id)
            { ans = confirm("Data akan di HAPUS?\n");
              if(ans)
              { document.mainform.action = "users/delete/" + id + " ";
                document.mainform.submit();
              }
            }
</script>
<form method=post name=mainform>
<script>document.mainform.name.focus();</script>
</form>
          <div class="row">
                <div class="col-lg-12">
                  <h3 class="page-header">Data Admin / Operator</h3>
                </div>
            </div>
            <div class="box-header">
                <?php
                echo anchor('users/baru','<i class="fa fa-plus-circle"></i> Tambah Data',array('class'=>'btn btn-default'));
                ?>
                                           
            </div>
            <br />
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        User Management</div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="50">No</th>
                                            <th width="150">Nama lengkap</th>
                                            <th width="150">Username</th>
                                            <th width="50">Level</th>
                                            <th width="100">Status</th>
                                            <th width="50">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
		$no=1;
                foreach ($record->result() as $m)
        { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $m->namaLengkap; ?></td>
            <td><?php echo $m->username; ?></td>
            <td><?php echo $m->level; ?> </td>
            <td><?php echo $m->status; ?></td>
            <td>
            <div align="center">
                <?php
                echo anchor('users/edit/'.$m->id,'<i class="glyphicon glyphicon-list"></i>');
                ?>
            
	<?php echo "<a href='javascript:godelete($m->id)'>"; ?><i class="glyphicon glyphicon-trash"></i></a></div></td>
        </tr>
         <?php $no++; } ?>
                                    </tbody>
                                </table>
                          </div>
                            <!-- /.table-responsive -->
                      </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
          </div>
