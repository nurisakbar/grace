<script> 
    function lengthRestriction(elem, min, max) {  
       var uInput = elem.value;  
          if(uInput.length >= min && uInput.length <= max){  
          return true;  
          } else {  
        alert("Password Minimal " +min+  " karakter");  
        elem.focus();  
               return false;  
          }  
    }  
</script>    
<div class="row">
<div class="col-lg-12">
  <h3 class="page-header">Manage User / Admin</h3>
</div>
</div>
<?php
echo form_open('users/profile',array('class'=>'form-horizontal'));
?>
<input name="id" value="<?php echo $r['id']?>" type="hidden">

<div class="form-group">
<label class="col-sm-2 control-label">Nama Lengkap</label>
<div class="col-md-3"><input name="nama" value="<?php echo $r['namaLengkap']?>" type="text" class="form-control" id="exampleInputEmail1">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Username</label><div class="col-md-3">
<input name="username" type="text" value="<?php echo $r['username']?>" class="form-control" id="exampleInputPassword1" />
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Password</label><div class="col-md-3">
<input name="password" type="password" class="form-control" id="restrict" onBlur="lengthRestriction(document.getElementById('restrict'), 8, 20)"/>
</div>
</div>

<?php if($this->session->userdata('level')=='Super Admin' || $this->session->userdata('level')=='Administrator')
{ ?>

<div class="form-group">
<label class="col-sm-2 control-label">Level User</label>
<div class="col-md-2">
    <?php
    $level=array(''=>'------','user'=>'User','Administrator'=>'Administrator','Super Admin'=>'Super Admin');
    echo form_dropdown('level',$level,$r['level'],"class='form-control'");
    ?>

</div>
</div>


<!-- input -->
<div class="form-group">
<label class="col-sm-2 control-label">Status user</label><div class="col-sm-2">
    <?php
    $aktif=array(''=>'------','Aktif'=>'Aktif','Non Aktif'=>'Non Aktif');
    echo form_dropdown('status',$aktif,$r['status'],"class='form-control'");
    ?>

</div>
</div>
                                        
<?php } ?>
<!-- input -->
<div class="form-group">
<label class="col-sm-2 control-label"></label>
<div class="col-sm-1">
    <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
</div>
</div>
</form>
