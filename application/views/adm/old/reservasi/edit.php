<link type="text/css" href="<?php echo base_url();?>assets/kalender/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/kalender/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/ui.core.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/ui.datepicker.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/i18n/ui.datepicker-id.js"></script>
    <script type="text/javascript"> 
      var $skpd = jQuery.noConflict();
      $skpd(document).ready(function(){
        $skpd("#awal").datepicker({
		  dateFormat  : "yy-mm-dd",        
          changeMonth : true				
        });
      });
      
      var $skpd = jQuery.noConflict();
      $skpd(document).ready(function(){
        $skpd("#akhir").datepicker({
		  dateFormat  : "yy-mm-dd",        
          changeMonth : true				
        });
      });
    </script>
    
<script type="text/javascript" src="<?php echo base_url();?>assets/mce/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
function ajaxfilemanager(field_name, url, type, win) {
   var ajaxfilemanagerurl = "<?php echo base_url()?>assets/mce/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
   switch (type) {
    case "image":
     break;
    case "media":
     break;
    case "flash": 
     break;
    case "file":
     break;
    default:
     return false;
   }
            tinyMCE.activeEditor.windowManager.open({
                url: "mce/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
             /*            return false;   
   var fileBrowserWindow = new Array();
   fileBrowserWindow["file"] = ajaxfilemanagerurl;
   fileBrowserWindow["title"] = "Ajax File Manager";
   fileBrowserWindow["width"] = "782";
   fileBrowserWindow["height"] = "440";
   fileBrowserWindow["close_previous"] = "no";
   tinyMCE.openWindow(fileBrowserWindow, {
     window : win,
     input : field_name,
     resizable : "yes",
     inline : "yes",
     editor_id : tinyMCE.getWindowArg("editor_id")
   });
   
   return false;*/
  }
</script>

<script type="text/javascript">
 tinyMCE.init({
  
  // General options
  mode : "textareas",
  elements : "ajaxfilemanager",
  file_browser_callback : 'ajaxfilemanager',
  theme : "advanced",
  plugins : "safari,pagebreak,style,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

  // Theme options
  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
 theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
 theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
 theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
 
  theme_advanced_toolbar_location : "top",
  theme_advanced_toolbar_align : "left",
  theme_advanced_statusbar_location : "bottom",
  theme_advanced_resizing : true,
  relative_urls : false,
  remove_script_host : false,
  // Example content CSS (should be your site CSS)
  content_css : "css/content.css",

  // Drop lists for link/image/media/template dialogs
  

  // Replace values for the template plugin
  template_replace_values : {
   username : "Some User",
   staffid : "991234"
  }
 });
</script>

<div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Tambah Reservasi</h3>
    </div>
</div>
<?php
echo form_open('adm/reservasi/edit',array('class'=>'form-horizontal'));
?>
<input type="hidden" name="id" value="<?php echo $r['reservasi_id']?>">

  <div class="form-group">
    <label class="col-sm-2 control-label">Tanggal Awal/ Akhir</label>
<div class="col-sm-2">
    <input name="tanggal_awal" placeholder="Tanggal Awal" value="<?php echo $r['tanggal_awal']?>"  required="required" class="form-control" id="awal"  />
      
    </div>
    <div class="col-sm-2">
      <input name="tanggal_akhir" value="<?php echo $r['tanggal_akhir']?>"  placeholder="Tanggal Akhir" required="required" class="form-control" id="akhir"  />
      
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Nama</label>
    <div class="col-md-6">
<input name="nama" placeholder="Nama Lengkap" value="<?php echo $r['nama']?>"  required="required" class="form-control" id="inputEmail3" />
    </div>
  </div>
  
    <div class="form-group">
    <label  class="col-sm-2 control-label">Telpon/ Email</label>
    <div class="col-md-4">
        <input name="telpon" placeholder="No Telpon"  value="<?php echo $r['telpon']?>" required="required" class="form-control" id="inputEmail3" />
    </div>
        <div class="col-md-4">
            <input name="email" type="email" value="<?php echo $r['email']?>"  placeholder="Email" required="required" class="form-control" id="inputEmail3" />
    </div>
  </div>
    <div class="form-group">
    <label  class="col-sm-2 control-label">Jenis</label>
    <div class="col-md-6">
        <?php
        $jenis=array('Ruang Standart'=>'Ruang Standart','Gedung Penerima  I'=>'Gedung Penerima  I','Gedung Penerima II'=>'Gedung Penerima II','Halaman Parkir'=>'Halaman Parkir');
        echo form_dropdown('jenis',$jenis,$r['jenis'],"class='form-control'");
        ?>
  
    </div>
  </div>
    <div class="form-group">
    <label  class="col-sm-2 control-label">Status</label>
    <div class="col-md-6">
        <?php
        $status=array('konfirmasi'=>'Publish','tidak'=>'Tidak');
        echo form_dropdown('status',$status,$r['status'],"class='form-control'");
        ?>
  
    </div>
  </div>

  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Keterangan</label>
    <div class="col-sm-6">
<textarea name="keterangan" rows="5" cols="100" style="width: 80%" id="isi"><?php echo $r['keterangan']?></textarea>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>