<style type="text/css" title="currentStyle">		
		@import "css/demo_table_jui.css";
		@import "css/jquery-ui-1.8.4.custom.css";
	</style>
	<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			oTable = $('#example').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
		} );
	</script>
<script>
function godelete(id)
            { ans = confirm("Data akan di HAPUS?\n");
              if(ans)
              { document.mainform.action = "reservasi/delete/" + id + " ";
                document.mainform.submit();
              }
            }
</script>
<form method=post name=mainform>
<script>document.mainform.name.focus();</script>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Reservasi</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="box-header">
    <?php
    echo anchor('adm/reservasi/baru','<i class="fa fa-plus-circle"></i> Tambah reservasi',array('class'=>'btn btn-default'));
    ?>
   </div>
<br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Lebar Header 1000 / Tinggi Menyesuaikan</div>
                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
<table width="100%" border="0" cellpadding="2" class="table table-striped table-bordered table-hover" id="dataTables-example">
  <thead>
  <tr>
    <th width="30" align="center" class="isitable">No</th>
    <th width="493" align="center" class="isitable">Nama</th>
    <th width="133" align="center" class="isitable">email</th>
    <th width="93" align="center" class="isitable">telpon</th>
    <th width="408" align="center" class="isitable">Jenis</th>
    <th width="150" align="center" class="isitable">Tanggal</th> 
    <th></th>
  </tr>
  </thead>
<?php
$nomer = 1;
foreach ($record->result() as $ban)
{
?>
  <tr>
    <td width="30" class="isitable"><?php echo $nomer; ?></td>
    <td width="393" class="isitable"><?php echo $ban->nama; ?></td>
        <td width="133" class="isitable"><?php echo $ban->email; ?></td>
            <td width="80" class="isitable"><?php echo $ban->telpon; ?></td>
    <td width="108" class="isitable"><?php echo $ban->jenis; ?></td>
    <td width="150" class="isitable"><?php echo tgl_indo($ban->tanggal_awal); ?></td>
    <td width="60" class="isitable"><div align="center">
             <?php echo anchor('adm/reservasi/edit/'.$ban->reservasi_id,' <i class="glyphicon glyphicon-list"></i>')?>                                  
        <?php echo "<a href='javascript:godelete($ban->reservasi_id)'>"; ?>
	<i class="glyphicon glyphicon-trash"></i></a></div></td>
  </tr>
  <?php
  $nomer = $nomer + 1;
  }
  ?>
</table>
</div>
<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>