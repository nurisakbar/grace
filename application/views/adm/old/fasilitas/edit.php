<link type="text/css" href="<?php echo base_url();?>assets/kalender/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/kalender/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/ui.core.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/ui.datepicker.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets/kalender/ui/i18n/ui.datepicker-id.js"></script>
    <script type="text/javascript"> 
	var $skpd = jQuery.noConflict();
      $skpd(document).ready(function(){
        $skpd("#awal").datepicker({
		  dateFormat  : "yy-mm-dd",        
          changeMonth : true				
        });
      });
    </script>
    
<script type="text/javascript" src="<?php echo base_url();?>assets/mce/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
function ajaxfilemanager(field_name, url, type, win) {
   var ajaxfilemanagerurl = "<?php echo base_url()?>assets/mce/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
   switch (type) {
    case "image":
     break;
    case "media":
     break;
    case "flash": 
     break;
    case "file":
     break;
    default:
     return false;
   }
            tinyMCE.activeEditor.windowManager.open({
                url: "mce/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
             /*            return false;   
   var fileBrowserWindow = new Array();
   fileBrowserWindow["file"] = ajaxfilemanagerurl;
   fileBrowserWindow["title"] = "Ajax File Manager";
   fileBrowserWindow["width"] = "782";
   fileBrowserWindow["height"] = "440";
   fileBrowserWindow["close_previous"] = "no";
   tinyMCE.openWindow(fileBrowserWindow, {
     window : win,
     input : field_name,
     resizable : "yes",
     inline : "yes",
     editor_id : tinyMCE.getWindowArg("editor_id")
   });
   
   return false;*/
  }
</script>

<script type="text/javascript">
 tinyMCE.init({
  
  // General options
  mode : "textareas",
  elements : "ajaxfilemanager",
  file_browser_callback : 'ajaxfilemanager',
  theme : "advanced",
  plugins : "safari,pagebreak,style,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

  // Theme options
  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
 theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
 theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
 theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
 
  theme_advanced_toolbar_location : "top",
  theme_advanced_toolbar_align : "left",
  theme_advanced_statusbar_location : "bottom",
  theme_advanced_resizing : true,
  relative_urls : false,
  remove_script_host : false,
  // Example content CSS (should be your site CSS)
  content_css : "css/content.css",

  // Drop lists for link/image/media/template dialogs
  

  // Replace values for the template plugin
  template_replace_values : {
   username : "Some User",
   staffid : "991234"
  }
 });
</script>

<div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Edit Content</h3>
    </div>
</div>
<?php
echo form_open('adm/content/edit',array('class'=>'form-horizontal'));
?>
<input type="hidden" name="id" value="<?php echo $r['id_content']?>">
  <div class="form-group">
    <label class="col-sm-2 control-label">Menu</label>
<div class="col-sm-2">
      <input name="menu" class="form-control" value="<?php echo $r['menu']?>" />
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Judul</label>
    <div class="col-md-6">
<input name="judul" class="form-control" value="<?php echo $r['judul']?>" id="inputEmail3" />
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Subtitle</label>
    <div class="col-md-6">
<input name="isi" class="form-control" id="inputEmail3" value="<?php echo $r['subtitle']?>" />
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Isi</label>
    <div class="col-sm-6">
<textarea name="isi" rows="15" cols="130" style="width: 80%" id="isi"><?php echo $r['isi']?></textarea>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>