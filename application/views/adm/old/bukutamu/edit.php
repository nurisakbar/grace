<div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Edit Buku Tamu</h3>
    </div>
</div>

<?php
echo form_open_multipart('bukutamu/edit',array('class'=>'form-horizontal'));
?>

  <div class="form-group">
    <label class="col-sm-2 control-label">Nama</label>
    <input name="id" type="hidden" id="id" value="<?php echo $r['id_tamu']; ?>" />
<div class="col-sm-2">
      <input name="nama" class="form-control" id="inputEmail3" value="<?php echo $r['nama']; ?>" disabled/>
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Email</label>
    <div class="col-md-6">
<input name="keterangan" class="form-control" id="inputEmail3" value="<?php echo $r['email']; ?>" disabled/>
    </div>
  </div>
  
    <div class="form-group">
    <label  class="col-sm-2 control-label">Pesan</label>
    <div class="col-md-6">
      <textarea name="pesan" rows="3" class="form-control" id="pesan" ><?php echo $r['pesan']; ?></textarea>
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-2 control-label">Tanggapan</label>
    <div class="col-md-6">
      <textarea name="tanggapan" rows="3" class="form-control" id="tanggapan" ><?php echo $r['tanggapan'];  ?></textarea>
    </div>
  </div>
  
  <!-- input -->
  <div class="form-group">
    <label class="col-sm-2 control-label">Status</label>
    <div class="col-sm-2">
        
        <?php
        $status=array('Blokir'=>'Blokir','Publish'=>'Publish');
        echo form_dropdown('status',$status,$r['status'],"class='form-control'");
        ?>
     
    </div>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>

