<style type="text/css" title="currentStyle">		
		@import "css/demo_table_jui.css";
		@import "css/jquery-ui-1.8.4.custom.css";
	</style>
	<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			oTable = $('#example').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
		} );
	</script>
<script>
function godelete(id)
            { ans = confirm("Data akan di HAPUS?\n");
              if(ans)
              { document.mainform.action = "banner/delete/" + id + " ";
                document.mainform.submit();
              }
            }
</script>
<form method=post name=mainform>
<script>document.mainform.name.focus();</script>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Buku Tamu</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Lebar Header 1000 / Tinggi Menyesuaikan</div>
                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
			<table width="100%" border="0" cellpadding="2" class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th width="75" style="width: 25px">No</th>
							<th width="220" style="width: 150px">Tanggal</th>
							<th width="220" style="width: 100px">Nama</th>
							<th width="672" style="width: 100px">Email</th>
							<th width="350" style="width: 350px">Pesan</th>
							<th width="150" style="width: 50px">Status</th>
							<th width="150" style="width: 50px">Action</th>
					  </tr>
					</thead>
					<tbody>
					<?php 
                    $no = 1;
                    foreach ($record->result() as $data)
                    {?>
                    <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo tgl_indo($data->tanggal); ?></td>
                    <td><?php echo $data->nama; ?></td>
                    <td><?php echo $data->email; ?></td>
                    <td><?php echo $data->pesan; ?></td>
                    <td><?php echo $data->status; ?></td>
                    <td>
                        <?php echo anchor('bukutamu/edit/'.$data->id_tamu,'<i class="glyphicon glyphicon-list"></i>');?>
                    <?php echo "<a href='javascript:godelete($data->id_tamu)'>"; ?>
                    <i class="glyphicon glyphicon-trash"></i></a></td>
						</tr>
						<?php $no++; } ?>                
					</tbody>
				</table>
</div>
<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>