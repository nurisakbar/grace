<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">TAMBAH BANNER</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
echo form_open_multipart('adm/banner/baru',array('class'=>'form-horizontal'));
?>

  <!-- input -->

  <div class="form-group">
    <label class="col-sm-2 control-label">Judul </label>
    <div class="col-sm-4">
      <input name="judul" class="form-control" />
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-2 control-label">Link Banner </label>
    <div class="col-sm-4">
      <input name="lingnya" class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Banner</label>
    <div class="col-sm-10">
      <input name="userfile"  type="file" size="35" id="userfile" />
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Keterangan </label>
    <div class="col-sm-4">
        <textarea name="keterangan" class="form-control" /> </textarea>
    </div>
  </div>
 <!-- BUTTON -->   
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
    <div class="col-sm-2">
        <button type="submit" name="submit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>SIMPAN</button>
    </div>
  </div>
</form>