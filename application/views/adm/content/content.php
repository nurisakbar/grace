<style type="text/css" title="currentStyle">		
		@import "css/demo_table_jui.css";
		@import "css/jquery-ui-1.8.4.custom.css";
	</style>
	<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			oTable = $('#example').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
		} );
	</script>
<script>
function godelete(id)
            { ans = confirm("Data akan di HAPUS?\n");
              if(ans)
              { document.mainform.action = "content/hapus/" + id + " ";
                document.mainform.submit();
              }
            }
</script>
<form method=post name=mainform>
<script>document.mainform.name.focus();</script>
<!-- Main content -->
<div class="row">
    <div class="col-lg-12"><h3 class="page-header">Content</h3></div>
</div>

            
			<div class="box-header">
                            <?php
    echo anchor('adm/content/baru','<i class="fa fa-plus-circle"></i> Tambah Content',array('class'=>'btn btn-default'));
    ?>
				             
			</div>
            <br />

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Content</div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <div class="table-responsive">
                            
				<table width="100%" border="0" cellpadding="2" class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th style="width: 10px">No</th>
							<th style="width: 300px">Judul Content</th>
                                                        <th style="width: 300px">Link</th>
							<th style="width: 100px">Action</th>
					  </tr>
					</thead>
					<tbody>
					<?php 
                    $no = 1;
                    foreach ($record->result() as $data)
                    {?>
                    <tr>
                    <td><?php echo $no; ?></td>

                    <td><?php echo $data->judul; ?></td>
                    <td><?php echo 'p/pages/'.$data->id_cont.'/'.$data->judul_seo; ?></td>
                     <td>
                                              <div align="center">
                                                  <?php echo anchor('adm/content/edit/'.$data->id_cont,' <i class="glyphicon glyphicon-list"></i>')?>                                     
	<?php echo "<a href='javascript:godelete($data->id_cont)'>"; ?>
	<i class="glyphicon glyphicon-trash"></i></a></div>      
                                
                      
                    
                    
                    </td>
						</tr>
						<?php $no++; } ?>                
					</tbody>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
</div>
</div>