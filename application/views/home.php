
  <div class="content">
       <div id="judul-ourservice">NEWS</div>
    <div class="slider lazy">
    
        
        <?php
        $berita=$this->db->query('select * from berita limit 8')->result();
        foreach ($berita as $b){
        ?>
      <div>
        <div class="image"><img data-lazy="<?php echo base_url();?>assets/berita/<?php echo $b->foto?>"/>
            <div class="kota">
                <h3><?php echo $this->session->userdata('lang')=='id'? substr($b->judul,0,23):$b->judul_eng;?> ...</h3>
              </div>
        </div>
      </div>
        <?php } ?>
      
    </div>
  </div>

<div id="ourservice">
    <div id="judul-ourservice">EVENT</div>
    <?php 
    $no = 1;
    $servis = $this->db->query("select * from event limit 4")->result();
    foreach ($servis as $ser)
    { ?>
    <div id="kol1">
    	<div class="the-box-over" id="over-<?php echo $no; ?>">
				<h1><?php echo $ser->judul; ?></h1>
<img src="assets/event/<?php echo $ser->foto;?>" width="227">
               <!-- <a href="business-landing-pages/agribusiness">   -->          
                    <div class="caption-over" id="capt-<?php echo $no; ?>">
                        <h2><?php echo $ser->judul; ?></h2>
                        <p><?php echo $ser->content; ?></p>
                    </div>
				<!--</a>		-->	
        </div>
    </div>
    <?php $no++; } ?>
    <div id="clr"></div>
  </div>


  <script src="<?php echo base_url();?>assets/js/jquery-1.11.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery-migrate-1.2.1.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/highlight.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/slick/slick.js"></script>
  <script type="text/javascript">
	$(document).ready(function() {
		$('.lazy').slick({
		lazyLoad: 'ondemand',
		slidesToShow: 4,
		slidesToScroll: 1
		});
	}); 
</script>





  <div id="ourservice">
    <div id="judul-ourservice">OUR SERVICE</div>
    <?php 
    $no = 1;
    $servis = $this->db->query("select * from service limit 8")->result();
    foreach ($servis as $ser)
    { ?>
    <div id="kol1">
    	<div class="the-box-over" id="over-<?php echo $no; ?>">
				<h1><?php echo $ser->judul; ?></h1>
<img src="assets/service/<?php echo $ser->foto;?>" width="227">
               <!-- <a href="business-landing-pages/agribusiness">   -->          
                    <div class="caption-over" id="capt-<?php echo $no; ?>">
                        <h2><?php echo $ser->judul; ?></h2>
                        <p><?php echo $ser->content; ?></p>
                    </div>
				<!--</a>		-->	
        </div>
    </div>
    <?php $no++; } ?>
    <div id="clr"></div>
  </div>
  
  
  
  <p>&nbsp;</p>
  
  
  <script type="text/javascript">
			$( document ).ready(function() {
			  
			   	$("#over-1").hover(
				   function() {
				     $("#capt-1").fadeIn(100);
				  	 $("#over-1 h1").hide();
				   },
				   function() {
				      $("#capt-1").fadeOut(100);
				  	  $("#over-1 h1").show();
				   }
				);

				$("#over-2").hover(
				   function() {
				     $("#capt-2").fadeIn(100);
				  	 $("#over-2 h1").hide();
				   },
				   function() {
				      $("#capt-2").fadeOut(100);
				  	  $("#over-2 h1").show();
				   }
				);

				$("#over-3").hover(
				   function() {
				     $("#capt-3").fadeIn(100);
				  	 $("#over-3 h1").hide();
				   },
				   function() {
				      $("#capt-3").fadeOut(100);
				  	  $("#over-3 h1").show();
				   }
				);

				$("#over-4").hover(
				   function() {
				     $("#capt-4").fadeIn(100);
				  	 $("#over-4 h1").hide();
				   },
				   function() {
				      $("#capt-4").fadeOut(100);
				  	  $("#over-4 h1").show();
				   }
				);

				$("#over-5").hover(
				   function() {
				     $("#capt-5").fadeIn(100);
				  	 $("#over-5 h1").hide();
				   },
				   function() {
				      $("#capt-5").fadeOut(100);
				  	  $("#over-5 h1").show();
				   }
				);

				$("#over-6").hover(
				   function() {
				     $("#capt-6").fadeIn(100);
				  	 $("#over-6 h1").hide();
				   },
				   function() {
				      $("#capt-6").fadeOut(100);
				  	  $("#over-6 h1").show();
				   }
				);
				
				$("#over-7").hover(
				   function() {
				     $("#capt-7").fadeIn(100);
				  	 $("#over-7 h1").hide();
				   },
				   function() {
				      $("#capt-7").fadeOut(100);
				  	  $("#over-7 h1").show();
				   }
				);
				
				$("#over-8").hover(
				   function() {
				     $("#capt-8").fadeIn(100);
				  	 $("#over-68 h1").hide();
				   },
				   function() {
				      $("#capt-8").fadeOut(100);
				  	  $("#over-8 h1").show();
				   }
				);
				
				$("#over-9").hover(
				   function() {
				     $("#capt-9").fadeIn(100);
				  	 $("#over-9 h1").hide();
				   },
				   function() {
				      $("#capt-9").fadeOut(100);
				  	  $("#over-9 h1").show();
				   }
				);
				$("#over-10").hover(
				   function() {
				     $("#capt-10").fadeIn(100);
				  	 $("#over-10 h1").hide();
				   },
				   function() {
				      $("#capt-10").fadeOut(100);
				  	  $("#over-10 h1").show();
				   }
				);

			

			});
</script>


  
  
  <p>&nbsp;</p>
</div>