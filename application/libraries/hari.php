<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hari {
		
	function nama_hari($tgl){
	
		$tanggal = substr($tgl,0,10);
		$namahari = date('l', strtotime($tanggal));

		if ($namahari == "Sunday") $namahari = "Minggu";
		else if ($namahari == "Monday") $namahari = "Senin";
		else if ($namahari == "Tuesday") $namahari = "Selasa";
		else if ($namahari == "Wednesday") $namahari = "Rabu";
		else if ($namahari == "Thursday") $namahari = "Kamis";
		else if ($namahari == "Friday") $namahari = "Jumat";
		else if ($namahari == "Saturday") $namahari = "Sabtu";
		 
		return $namahari;
		}
 }