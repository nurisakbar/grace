<?php
class mod_reservasi extends ci_model{
    
    
    
    function tampilkan_data(){
        return $this->db->query('select * from reservasi order by created desc');
    }
    
    function tampil_depan(){
        $n=3;
        // menentukan timestamp n bulan berikutnya dari tanggal hari ini
        $nextN = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")+$n, date("Y")));
// menentukan timestamp n bulan sebelumnya dari tanggal hari ini
        $prevN = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-$n, date("Y")));

        return $this->db->query("select * from reservasi where status='konfirmasi' and tanggal_awal between '$prevN' and '$nextN' order by created desc");
    }
}