<?php
class MBerita extends ci_model{
       
    function tampil_semua()
    {
		$query= "SELECT * FROM berita ORDER BY id_berita DESC LIMIT 0,6";
        return $this->db->query($query);
    }
	
	function tampil_depan()
    {
		$query= "SELECT * FROM berita ORDER BY id_berita DESC LIMIT 0,3";
        return $this->db->query($query);
    }
    
    function get_one($id)
    {
        $param  =   array('id_berita'=>$id);
        return $this->db->get_where('berita',$param);
    }
    
}