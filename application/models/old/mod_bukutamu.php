<?php
class mod_bukutamu extends CI_Model{
    
    var $table="buku_tamu";
    
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function update(){
        $data=array('pesan'=>$this->input->post('pesan'),
                    'tanggapan'=>$this->input->post('tanggapan'),
                    'status'=>$this->input->post('status'));
        $this->db->where('id_tamu',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}