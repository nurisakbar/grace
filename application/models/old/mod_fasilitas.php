<?php
class mod_fasilitas extends ci_model{
    
    
    
    function tampilkan_data(){
        return $this->db->get('fasilitas');
    }
    
    function baru($filename){
        $data=array(
            'judul'     =>  $this->input->post('judul'),
            'menu_order'=>  $this->input->post('menu'),
            'foto'      =>  $filename,
            'isi'       =>  $this->input->post('isi'));
        $this->db->insert('fasilitas',$data);
    }
    
    function update(){
            $data=array(
            'judul'     =>  $this->input->post('judul'),
            'menu'     =>  $this->input->post('menu'),
            'subtitle'  =>  $this->input->post('judul'),
            'isi'       =>  $this->input->post('isi'));
        $this->db->where('id_fasilitas',$this->input->post('id'));
        $this->db->update('fasilitas',$data);
    }
}