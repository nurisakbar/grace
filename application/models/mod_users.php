<?php
class mod_users extends CI_Model{
    
    var $tables         ="admin";
    
    function chek_login(){
        $username   =  $this->input->post('user',TRUE);
        $password   =  $this->input->post('pass',TRUE);
        $param      =  array('username'=>$username,'password'=>  md5($password));
        $chek       =  $this->db->get_where($this->tables,$param);
        if($chek->num_rows()>0){
            return $chek->row_array();
        }else{
            return 0;
        }
    }
    
    function baru(){
         $data=array('username'     =>  $this->input->post('username'),
                     'namaLengkap'  =>  $this->input->post('nama'),
                     'level'        =>  $this->input->post('level'),
                     'password'     =>  md5($this->input->post('password')),
                     'status'       =>  $this->input->post('status'));
          $this->db->insert('users',$data);
    }
    
    
    function edit(){
        
    }
    
    function update_profile(){
        $password=$this->input->post('password');
        if($password==''){
                $data=array('username'=>  $this->input->post('username'),
                            'namaLengkap'=>$this->input->post('nama'),
                            'level'=>$this->input->post('level'),
                            'status'=>$this->input->post('status'));
        }else{
                $data=array('username'=>  $this->input->post('username'),
                            'namaLengkap'=>$this->input->post('nama'),
                            'level'=>$this->input->post('level'),
                            'password'=>md5($password),
                            'status'=>$this->input->post('status'));
        }
        $this->db->where('id',  $this->input->post('id'));
        $this->db->update('users',$data);
    }
    
    function get_all(){
        return $this->db->get($this->tables);
    }
    
    
}