<?php
class mod_menu extends CI_Model{
    
    var $table="menu";
    
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function baru(){
        $data=array(
                    'title'=>$this->input->post('judul'),
                    'title_eng'=>$this->input->post('judul_eng'),
                    'url'=>  $this->input->post('lingnya'));
        $this->db->insert($this->table,$data);
    }
    
    
    function update($file_name){
        $data=array(
                    'title'=>$this->input->post('judul'),
                    'title_eng'=>$this->input->post('judul_eng'),
                    'url'=>  $this->input->post('lingnya'));
        $this->db->where('id_menu',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}