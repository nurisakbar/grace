<?php
class mod_submenu extends CI_Model{
    
    var $table="submenu";
    
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function baru(){
        $data=array(
                    'title'=>$this->input->post('judul'),
                    'id_menu'=>$this->input->post('menu'),
                    'title_eng'=>$this->input->post('judul_eng'),
                    'url'=>  $this->input->post('lingnya'));
        $this->db->insert($this->table,$data);
    }
    
    
    function update($file_name){
        $data=array(
                    'title'=>$this->input->post('judul'),
                    'title_eng'=>$this->input->post('judul_eng'),
            'id_menu'=>$this->input->post('menu'),
                    'url'=>  $this->input->post('lingnya'));
        $this->db->where('id_submenu',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}