<?php
class mod_subsubmenu extends CI_Model{
    
    var $table="subsubmenu";
    
    
    function tampilkan_data(){
        $sql="SELECT ssb.*,sb.title FROM subsubmenu as ssb,submenu as sb WHERE sb.id_submenu=ssb.id_submenu";
        return $this->db->query($sql);
    }
    
    function baru(){
        $data=array(
                    'judul'=>$this->input->post('judul'),
                    'id_submenu'=>$this->input->post('menu'),
                    'judul_eng'=>$this->input->post('judul_eng'),
                    'link'=>  $this->input->post('lingnya'));
        $this->db->insert($this->table,$data);
    }
    
    
    function update(){
        $data=array(
                    'judul'=>$this->input->post('judul'),
                    'judul_eng'=>$this->input->post('judul_eng'),
                    'id_submenu'=>$this->input->post('menu'),
                    'link'=>  $this->input->post('lingnya'));
        $this->db->where('id_subsubmenu',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}