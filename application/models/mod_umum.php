<?php
class mod_umum extends CI_Model{
    
    var $db1="";
    
    function __construct() {
        parent::__construct();
        $this->db1=  $this->load->database('default',TRUE);
    }
    
    function delete($table,$pk,$id){
        $this->db->where($pk,$id);
        $this->db->delete($table);
    }
    
    function get_one($table,$pk,$id){
        return $this->db->get_where($table,array($pk=>$id));
    }
    
    function get_content($id){
        return $this->db->get_where('content',array('id_cont'=>$id));
    }
    
    function update_content(){
        $data=array('judul'=> $this->input->post('judul'),
                    'isi'=> $this->input->post('isi'));
        $this->db->where('id_cont',  $this->input->post('id'));
        $this->db->update('content',$data);
    }
    
    function update_status_modulle($id,$status){
        $this->db->where('id_modulle',$id);
        $this->db->update('modulle',array('status'=>$status));
    }
    
    
    function upload_pdf_module($filename){
        $this->db->query("update page_pdf set status='0' where id_menu='".$this->input->post('id')."'");
        $data=array('tahun'=>$this->input->post('tahun'),
                    'judul'=>$this->input->post('judul'),
                    'id_menu'=>$this->input->post('id'),
                    'file_pdf'=>$filename,'status'=>1);
         $this->db->insert('page_pdf',$data);
    }
    
    function konfigurasi(){
        return $this->db->get_where('konfigurasi',array('id_skpd'=>20));
    }
    

    
    
    
    function update_config(){
                    $data=array(
                    'title'     =>  $this->input->post('title'),
                    'keyword'   =>  $this->input->post('key'),
                    'deskripsi' =>  $this->input->post('deskripsi'),
                    'nama_comp' =>  $this->input->post('nama_comp'),
                    'website'   =>  $this->input->post('website'),
                    'alamat'    =>  $this->input->post('alamat'),
                    'email'     =>  $this->input->post('email'),
                    'telp'      =>  $this->input->post('telp'),
                    'fax'       =>  $this->input->post('fax'));
                $this->db->where('id',1);
                $this->db->update('konfigurasi',$data);
    }
}