<?php
class mod_content extends CI_Model{
    
    var $table="content";
    
    function content_depan(){
     $sql="select * from content limit 7";
     return $this->db->query($sql);
    }
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function baru(){
        $data=array('tanggal'   =>  waktu(), 
                    'judul'     =>  $this->input->post('judul'),
                    'judul_eng'     =>  $this->input->post('judul_eng'),
                    'judul_seo' => seo_title($this->input->post('judul')),
                    'isi' =>  $this->input->post('isi'),
                    'isi_eng' =>  $this->input->post('isi_eng'));
        $this->db->insert($this->table,$data);
        
    }
    
    function update(){
                            $data=array(
                    'judul'     =>  $this->input->post('judul'),
                    'judul_eng'     =>  $this->input->post('judul_eng'),
                    'judul_seo' => seo_title($this->input->post('judul')),
                    'isi' =>  $this->input->post('isi'),
                    'isi_eng' =>  $this->input->post('isi_eng'));
        $this->db->where('id_cont',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}