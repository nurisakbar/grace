<?php
class mod_banner extends CI_Model{
    
    var $table="banner";
    
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function baru($file_name){
        $data=array('foto'=>$file_name,
                    'title'=>$this->input->post('judul'),
                    'deskripsi'=>$this->input->post('keterangan'),
                    'link'=>  $this->input->post('lingnya'));
        $this->db->insert($this->table,$data);
    }
    
    
    function update($file_name){
        if($file_name!=''){
                    $data=array('foto'=>$file_name,
                    'title'=>$this->input->post('judul'),
                    'link'=>  $this->input->post('lingnya'));
        }else{
                    $data=array(
                     'title'=>$this->input->post('judul'),
                    'link'=>  $this->input->post('lingnya'));
        }
        $this->db->where('id_banner',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}