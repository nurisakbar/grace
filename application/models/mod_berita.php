<?php
class mod_berita extends CI_Model{
    
    var $table="berita";
    
    function berita_depan(){
     $sql="select * from berita limit 7";
     return $this->db->query($sql);
    }
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function baru($file_name){
        $data=array('tanggal'   =>  waktu(),
                    'foto'      =>  $file_name,
                    'judul'     =>  $this->input->post('judul'),
                    'judul_eng'     =>  $this->input->post('judul_eng'),
                    'judul_seo' => seo_title($this->input->post('judul')),
                    'isi' =>  $this->input->post('isi'),
                    'isi_eng' =>  $this->input->post('isi_eng'));
        $this->db->insert($this->table,$data);
        
    }
    
    function update($file_name){
        if($file_name==''){
                    $data=array(
                    'judul'     =>  $this->input->post('judul'),
                    'judul_eng'     =>  $this->input->post('judul_eng'),
                    'judul_seo' => seo_title($this->input->post('judul')),
                    'isi' =>  $this->input->post('isi'),
                    'isi_eng' =>  $this->input->post('isi_eng'));
        }else{
                    $data=array(
                    'foto'      =>  $file_name,
                    'judul'     =>  $this->input->post('judul'),
                    'judul_seo' => seo_title($this->input->post('judul')),
                    'isi' =>  $this->input->post('isi'),
                    'isi_eng' =>  $this->input->post('isi_eng'));
        }
        $this->db->where('id_berita',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
}