<?php
class mod_profile extends CI_Model{
    
    var $table="profile";
    
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function baru($img){
        $data=array(
                    'tahun'=>$this->input->post('tahun'),
                    'content'=>$this->input->post('isi'),
                    'gambar'=>$img,
                    'content_eng'=>  $this->input->post('isi_eng'));
        $this->db->insert($this->table,$data);
    }
    
    
    function update($file_name){
        if(empty($file_name)){
                    $data=array(
                    'tahun'=>$this->input->post('tahun'),
                    'content'=>$this->input->post('isi'),
                    'content_eng'=>  $this->input->post('isi_eng'));
        }else{
        $data=array(
                    'tahun'=>$this->input->post('tahun'),
                    'content'=>$this->input->post('isi'),
                    'gambar'=>$file_name,
                    'content_eng'=>  $this->input->post('isi_eng'));
    }
        $this->db->where('id_profile',  $this->input->post('id'));
        $this->db->update($this->table,$data);
    }
    
}