<?php
class mod_galleryfoto extends CI_Model{
    
    var $table="gallery";
    
    
    function tampilkan_data(){
        return $this->db->get($this->table);
    }
    
    function upload($image_file){
        $data=array(
            'title'=>  $this->input->post('title'),
            'title_eng'=>  $this->input->post('title_eng'),
             'deskripsi' =>  $this->input->post('isi'),
             'deskripsi_eng' =>  $this->input->post('isi_eng'),
            'foto'=>$image_file);
        $this->db->insert($this->table,$data);
    }
    
    function edit($image_file){
        if($image_file!=''){
                            $data=array(
                'title'=>  $this->input->post('title'),
                'title_eng'=>  $this->input->post('title_eng'),
                'deskripsi' =>  $this->input->post('isi'),
                'deskripsi_eng' =>  $this->input->post('isi_eng'),
                'foto'=>$image_file);
        }else{
                            $data=array(
                'title'=>  $this->input->post('title'),
                'title_eng'=>  $this->input->post('title_eng'),
                'deskripsi' =>  $this->input->post('isi'),
                'deskripsi_eng' =>  $this->input->post('isi_eng'));
        }
                $this->db->where('id_gallery',  $this->input->post('id'));
        $this->db->update($this->table,$data);

    }
}