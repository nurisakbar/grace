<?php
class p extends ci_controller{
    
    
    function index(){
        $this->template->load('template','home');
    }
    
    function pages(){
         $data['berita']=  $this->db->query('select * from berita limit 5');
         $id=  $this->uri->segment(3);
         $data['detail']=  $this->db->get_where('content',array('id_cont'=>$id))->row_array();
         $this->template->load('template','pages',$data);
    }
}