<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model(array('mberita','mod_reservasi'));
    }
	
	public function index()
	{

		$kode	= 4;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "profil";
		$data['judul']      = "Jadwal Pengunaan Gedung";
								// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
                $data['jadwal']=  $this->mod_reservasi->tampil_depan()->result();
		$this->template->load('template','jadwal',$data);
	}	
}