<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model('mberita');
    }
	
	public function index()
	{	
            if(isset($_POST['submit'])){
                $data=array('nama'=>  $this->input->post('nama',TRUE),
                    'email'=>$this->input->post('email',TRUE),
                    'comment'=>$this->input->post('comment',TRUE),
                    'tanggal'=>  waktu());
                $this->db->insert('kontak',$data);
                $data['aktif']      = "kontak";
		$data['judul']      = "Kontak";
		$data['breadcrumb1_tujuan']	= "terima kasih sudah menghubungi kami"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
		$this->template->load('template','contact',$data);
            }else{
                $kode	= 1;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "kontak";
		$data['judul']      = "Kontak";
		
		$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
		$this->template->load('template','contact',$data);
            }
	}	
}