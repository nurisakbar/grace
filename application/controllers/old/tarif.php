<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tarif extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model('mberita');
    }
	
	public function index()
	{	
		$kode	= 3;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "tarif";
		$data['judul']      = "Tarif";
		
		$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
		$this->template->load('template','tarif',$data);
	}	
}