<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model('MBerita');
		$this->load->model('konfigurasi');
		$this->load->model('gallery');
    }
	
	public function index()
	{		
		$data['konfigur']   = $this->konfigurasi->get_conf(1)->row_array();
		$data['gallery']     =  $this->gallery->tampil6();
		$data['record']      =  $this->MBerita->tampil_depan();
		$data['judul']      =  "Pemerintah Kota Balikpapan";
		$this->load->view('homepage', $data);
	}

	public function content()
	{
		$id=  $this->uri->segment(2);
		$data['gallery']    = $this->gallery->tampil9();
		$data['record']     = $this->content->get_one($id)->row_array();
		$induk				= $data['record']['judul'];	
		$data['induk']      = $this->content->get_parent($induk)->row_array();
		if($data['induk']['id'] != '0')
		{
			$data['breadcrumb1']		= $data['induk']['id'];
		}
		$this->template->load('template','web/menuicon',$data);
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */