<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservasi extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model('mberita');
                $this->load->library('session');
    }
	
	public function index()
	{	
            if(isset($_POST['submit'])){
                $data=array('nama'=>  $this->input->post('nama',TRUE),
                    'telpon'=>$this->input->post('telpon',TRUE),
                    'email'=>$this->input->post('email',TRUE),
                    'jenis'=>$this->input->post('jenis',TRUE),
                    'tanggal_awal'=>$this->input->post('tanggal_awal',TRUE),
                    'tanggal_akhir'=>$this->input->post('tanggal_akhir',TRUE),
                    'status'=>'tidak',
                    'keterangan'=>$this->input->post('keterangan',TRUE),
                    'created'=>  waktu());
                $this->db->insert('reservasi',$data);
                $this->session->set_flashdata(array('pesan'=>'Reservasi Berhasil, Kami Akan menghubungi anda secepatnya'));
                redirect('reservasi');
            }else{
                $kode	= 3;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "reservasi";
		$data['judul']      = "Reservasi";
		
		$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
		$this->template->load('template','reservasi',$data);
            }
	}	
}