<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gallery extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model(array('mberita','mod_galleryfoto'));
    }
	
	public function index()
	{	
		$kode	= 1;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "update";
		$data['judul']      = "gallery";
		
		$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
                $data['gallery']=  $this->mod_galleryfoto->tampilkan_data()->result();
		$this->template->load('template','gallery',$data);
	}	
}