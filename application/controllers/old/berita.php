<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('content');
		$this->load->model('mberita');
    }
	
	public function index()
	{	
		$kode	= 1;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "update";
		$data['judul']      = "Berita";
		
		$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
		$this->template->load('template','berita',$data);
	}
        
        
        function baca(){
                $kode	= 1;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); 	//mengambil data dari tabel Content
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "update";
                $berita=  $this->mod_umum->get_one('berita','id_berita',  $this->uri->segment(3))->row_array();
		$data['judul']      = $berita['judul_seo'];
                $this->update_hits($this->uri->segment(3));
            	$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
                $data['berita']=  $this->mod_umum->get_one('berita','id_berita',  $this->uri->segment(3))->row_array();
		$this->template->load('template','bacaberita',$data);
        }
        
        function update_hits($id){
            $r=  $this->db->get_where('berita',array('id_berita'=>$id))->row_array();
            $new_hits=$r['hits']+1;
            $this->db->where('id_berita',$id);
            $this->db->update('berita',array('hits'=>$new_hits));
        }
}