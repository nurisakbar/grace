<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fasilitas extends CI_Controller {


	function __construct() 
	{
        parent::__construct();
        $this->load->model('mfasilitas');
		$this->load->model('mberita');
		$this->load->model('content');
    }
	
	public function index()
	{	
		$kode	= 2;
		$data['record']     = $this->content->ambildata_content($kode)->row_array(); //mengambil data dari tabel Content
		$data['fasilitas']     = $this->mfasilitas->tampil_semua(); 
		$data['rberita']    = $this->mberita->tampil_depan();
		$data['aktif']      = "fasilitas";
		$data['judul']      = "Fasilitas";	
		
		$data['breadcrumb1_tujuan']	= "profil"; 							// alamat INDUK (profile) sesuai id_content
		//$data['breadcrumb1']		= $data['breadinduk']['title'];
		$this->template->load('template','fasilitas',$data);
	}	
}