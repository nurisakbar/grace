<?php
class News extends ci_controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('mod_berita');
    }
    
    function index(){      
        $id=  $this->uri->segment(3);
        if($id==''){
            $sql="SELECT id_berita FROM `berita` order by id_berita desc limit 1";
            $sql=  $this->db->query($sql)->row_array();
            $id=$sql['id_berita'];
        }else{
            $id=$id;
        }
        $data['berita']=$this->db->query('select * from berita limit 8');
        $data['detail']=  $this->db->get_where('berita',array('id_berita'=>$id))->row_array();
        $this->template->load('template','news',$data);
    }
    
    
    function search(){
        $id=  $this->uri->segment(3);
        $key =  $this->input->post('keyword');
        if($id==''){
            $sql="select * from berita where judul like '%$key%' limit 1";
            $sql=  $this->db->query($sql)->row_array();
            $id=$sql['id_berita'];
        }else{
            $id=$id;
        }
        $data['berita']=$this->db->query("select * from berita where judul like '%$key%' limit 8");
        $data['detail']=  $this->db->get_where('berita',array('id_berita'=>$id))->row_array();
        $this->template->load('template','news',$data);
    }
}