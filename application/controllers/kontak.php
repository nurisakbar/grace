<?php
class Kontak extends ci_controller{
    
    
    function index(){
        if(isset($_POST['submit'])){
            $this->load->model('mod_kontak');
            if($this->session->userdata('cap')==$this->input->post('saring')){
                $this->mod_kontak->kirim();
                $data['pesan']="<p class='isitabel'>terima kasih sudah menghubungi kami</p>";
            }else{
                $data['pesan']="<p class='isitabel'>Kode Yang Anda Masukan Salah</p>";
            }
            die;
            $this->template->load('template','kontak',$data);
        }else{
            $data['pesan']="";
            $this->load->helper('captcha');
            $random = substr( md5(rand()), 0, 7);
            $this->session->set_userdata('cap',$random);
            $vals = array(
            'word'	=> $random,
            'img_path'	=> './captcha/',
            'img_url'	=> base_url().'captcha/',
            //'font_path'	=> './path/to/fonts/texb.ttf',
            'img_width'	=> '150',
            'img_height' => 30,
            'expiration' => 7200
            );
            $cap = create_captcha($vals);
            $data['images']=$cap['image'];
            $this->template->load('template','kontak',$data);
        }
    }
}