<?php
class gallery extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_galleryfoto'));
    }
    
    function index(){
        $data['record']=  $this->mod_galleryfoto->tampilkan_data();
        $this->template->load('adm/template', 'adm/gallery/galleryfoto',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('gallery','id_gallery',$id);
        redirect('adm/gallery');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/gallery/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_galleryfoto->upload($data['file_name']);
             if($data['file_name']!=''){
             UploadImage($data['file_name'], 'gallery');}
            redirect('adm/gallery/baru');
        }else{
            $this->template->load('adm/template', 'adm/gallery/manage');
        }   
    }
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/gallery/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_galleryfoto->edit($data['file_name']);
             if($data['file_name']!=''){
             UploadImage($data['file_name'], 'gallery');}
            redirect('adm/gallery');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('gallery','id_gallery',$id)->row_array();
            $this->template->load('adm/template', 'adm/gallery/edit',$data);
        }   
    }
}