<?php
class submenu extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_submenu'));
    }
    
    function index(){
        $data['record']=  $this->mod_submenu->tampilkan_data();
        $this->template->load('adm/template', 'adm/submenu/submenu',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('submenu','id_submenu',$id);
        redirect('adm/submenu');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $this->mod_submenu->baru();
            redirect('adm/submenu');
        }else{
            $this->template->load('adm/template', 'adm/submenu/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_submenu->update();
            redirect('adm/submenu');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('submenu','id_submenu',$id)->row_array();
            $this->template->load('adm/template', 'adm/submenu/edit',$data);
        }   
    }
}