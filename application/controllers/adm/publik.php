<?php
class publik extends ci_controller{
    
    function __construct() {
        
        parent::__construct();
        chek_login();
    }
    
    function dashboard(){
        $this->template->load('adm/template', 'adm/dashboard');
    }
    
    function konfig(){
        if(isset($_POST['submit'])){
                        $this->mod_umum->update_config();
                        redirect('adm/publik/konfig');
        }else{
            $data['tek']=  $this->mod_umum->get_one('konfigurasi','id',1)->row_array();
            $this->template->load('adm/template', 'adm/konfig',$data);
        }
    }
    

}
