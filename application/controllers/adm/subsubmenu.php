<?php
class subsubmenu extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_subsubmenu'));
    }
    
    function index(){
        $data['record']=  $this->mod_subsubmenu->tampilkan_data();
        $this->template->load('adm/template', 'adm/subsubmenu/subsubmenu',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('subsubmenu','id_subsubmenu',$id);
        redirect('adm/subsubmenu');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $this->mod_subsubmenu->baru();
            redirect('adm/subsubmenu');
        }else{
            $this->template->load('adm/template', 'adm/subsubmenu/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_subsubmenu->update();
            redirect('adm/subsubmenu');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('subsubmenu','id_subsubmenu',$id)->row_array();
            $this->template->load('adm/template', 'adm/subsubmenu/edit',$data);
        }   
    }
}