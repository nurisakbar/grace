<?php
class content extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_content'));
    }
    
    function index(){
        $data['record']=  $this->mod_content->tampilkan_data();
        $this->template->load('adm/template', 'adm/content/content',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('content','id_content',$id);
        redirect('adm/content');
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_content->update();
            redirect('adm/content');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('content','id_content',$id)->row_array();
            $this->template->load('adm/template', 'adm/content/edit',$data);
        }
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $this->mod_content->baru();
            redirect('adm/content');
        }else{
            $this->template->load('adm/template', 'adm/content/manage'); 
        }
    }
    

}