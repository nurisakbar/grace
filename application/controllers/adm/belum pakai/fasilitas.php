<?php
class fasilitas extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_fasilitas'));
    }
    
    function index(){
        $data['record']=  $this->mod_fasilitas->tampilkan_data();
        $this->template->load('adm/template', 'adm/fasilitas/fasilitas',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $file=  $this->db->get_where('fasilitas',array('id_fasilitas'=>$id))->row_array();
        $file=$file['foto'];
        unlink('assets/images/'.$file);
        unlink('assets/images/small_'.$file);
        unlink('assets/images/medium_'.$file);
        $this->mod_umum->delete('fasilitas','id_fasilitas',$id);
        redirect('adm/fasilitas');
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_fasilitas->update();
            redirect('adm/fasilitas');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('fasilitas','id_fasilitas',$id)->row_array();
            $this->template->load('adm/template', 'adm/fasilitas/edit',$data);
        }
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/images/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_fasilitas->baru($data['file_name']);
            UploadImage($data['file_name'],'images');
            redirect('adm/fasilitas');
        }else{
            $this->template->load('adm/template', 'adm/fasilitas/manage'); 
        }
    }
    

}