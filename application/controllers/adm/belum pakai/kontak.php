<?php
class kontak extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_kontak'));
    }
    
    function index(){
        $data['record']=  $this->mod_kontak->tampilkan_data();
        $this->template->load('adm/template', 'adm/kontak/kontak',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('kontak','id',$id);
        redirect('adm/kontak');
    }
    

}