<?php
class reservasi extends CI_Controller{
    
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model('mod_reservasi');
    }
    
    
    function index(){
        $data['record']=  $this->mod_reservasi->tampilkan_data();
        $this->template->load('adm/template', 'adm/reservasi/reservasi',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('reservasi','reservasi_id',$id);
        redirect('adm/reservasi');
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
                     $data=array(
                        'nama'          =>  $this->input->post('nama',TRUE),
                        'telpon'        =>  $this->input->post('telpon',TRUE),
                        'email'         =>  $this->input->post('email',TRUE),
                        'jenis'         =>  $this->input->post('jenis',TRUE),
                        'tanggal_awal'  =>  $this->input->post('tanggal_awal',TRUE),
                        'tanggal_akhir' =>  $this->input->post('tanggal_akhir',TRUE),
                        'status'        =>  $this->input->post('status',TRUE),
                        'keterangan'    =>  $this->input->post('keterangan',TRUE));
                    $this->db->where('reservasi_id',$_POST['id']);
                $this->db->update('reservasi',$data);
                redirect('adm/reservasi');
        }else{
                    $data['r']=  $this->mod_umum->get_one('reservasi','reservasi_id',  $this->uri->segment(4))->row_array();
                    $this->template->load('adm/template', 'adm/reservasi/edit',$data);
        }
    }
    
    
    function baru(){
        if(isset($_POST['submit'])){
                     $data=array('nama'=>  $this->input->post('nama',TRUE),
                    'telpon'=>$this->input->post('telpon',TRUE),
                    'email'=>$this->input->post('email',TRUE),
                    'jenis'=>$this->input->post('jenis',TRUE),
                    'tanggal_awal'=>$this->input->post('tanggal_awal',TRUE),
                    'tanggal_akhir'=>$this->input->post('tanggal_akhir',TRUE),
                    'status'=>'konfirmasi',
                    'keterangan'=>$this->input->post('keterangan',TRUE),
                    'created'=>  waktu());
                $this->db->insert('reservasi',$data);
                redirect('adm/reservasi');
        }else{
                    $this->template->load('adm/template', 'adm/reservasi/manage');
        }
    }
}