<?php
class bukutamu extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_bukutamu'));
    }
    
    function index(){
        $data['record']=  $this->mod_bukutamu->tampilkan_data();
        $this->template->load('template', 'bukutamu/bukutamu',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(3);
        $this->mod_umum->delete('bukutamu','id_tamu',$id);
        redirect('bukutamu');
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_bukutamu->update();
            redirect('bukutamu');
        }else{
            $id=  $this->uri->segment(3);
            $data['r']=  $this->mod_umum->get_one('buku_tamu','id_tamu',$id)->row_array();
            $this->template->load('template', 'bukutamu/edit',$data);   
        }
    }
    

}