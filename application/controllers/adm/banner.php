<?php
class banner extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_banner'));
    }
    
    function index(){
        $data['record']=  $this->mod_banner->tampilkan_data();
        $this->template->load('adm/template', 'adm/banner/banner',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $file=  $this->db->get_where('banner',array('id_banner'=>$id))->row_array();
        $file=$file['foto'];
        unlink('assets/banner/'.$file);
        unlink('assets/banner/small_'.$file);
        unlink('assets/banner/medium_'.$file);
        $this->mod_umum->delete('banner','id_banner',$id);
        redirect('adm/banner');
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/banner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_banner->update($data['file_name']);
            if($data['file_name']!=''){
                UploadImage($data['file_name'], 'banner');
            }
            redirect('adm/banner');
        }else{
             $id=  $this->uri->segment(4);
             $data['r']=  $this->mod_umum->get_one('banner','id_banner',$id)->row_array();
             $this->template->load('adm/template', 'adm/banner/edit',$data);
        }
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/banner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
     
            $this->mod_banner->baru($data['file_name']);
            UploadImage($data['file_name'], 'banner');
            redirect('adm/banner');
        }else{
            $this->template->load('adm/template', 'adm/banner/manage');
        }   
    }
}