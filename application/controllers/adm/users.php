<?php
class users extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_users'));
    }
    
    function index(){
        $data['record']=  $this->mod_users->get_all();
        $this->template->load('template', 'users/users',$data);
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $this->mod_users->baru();
            redirect('users');
        }else{
            $this->template->load('template', 'users/manage');
        }
        
    }
    
    
    function delete(){
        $id=  $this->uri->segment(3);
        $this->mod_umum->delete('users','id',$id);
        redirect('users');
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_users->update_profile();
            redirect('users');
        }else{
            $id=  $this->session->userdata('id');
            $data['r']=  $this->mod_umum->get_one('users','id',$id)->row_array();
            $this->template->load('template', 'users/edit',$data);
        }
    }
    
    
    function profile(){
        if(isset($_POST['submit'])){
            $this->mod_users->update_profile();
            redirect('users/profile/'.$_POST['id']);
        }else{
            $id=  $this->session->userdata('id');
            $data['r']=  $this->mod_umum->get_one('users','id',$id)->row_array();
            $this->template->load('template', 'users/profile',$data);
        }
    }
}