<?php
class menu extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_menu'));
    }
    
    function index(){
        $data['record']=  $this->mod_menu->tampilkan_data();
        $this->template->load('adm/template', 'adm/menu/menu',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('menu','id_menu',$id);
        redirect('adm/menu');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $this->mod_menu->baru();
            redirect('adm/menu');
        }else{
            $this->template->load('adm/template', 'adm/menu/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->mod_menu->update();
            redirect('adm/menu');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('menu','id_menu',$id)->row_array();
            $this->template->load('adm/template', 'adm/menu/edit',$data);
        }   
    }
}