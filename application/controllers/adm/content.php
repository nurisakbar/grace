<?php
class content extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_content'));
    }
    
    function index(){
        $data['record']=  $this->mod_content->tampilkan_data();
        $this->template->load('adm/template', 'adm/content/content',$data);
    }
    
    function hapus(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('content','id_content',$id);
        redirect('adm/content');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $this->mod_content->baru();
            redirect('adm/content');
        }else{
            $this->template->load('adm/template', 'adm/content/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/content/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_content->update($data['file_name']);
                     if($data['file_name']!=''){
             UploadImage($data['file_name']);}
            redirect('adm/content');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('content','id_cont',$id)->row_array();
            $this->template->load('adm/template', 'adm/content/edit',$data);
        }   
    }
}