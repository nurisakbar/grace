<?php
class event extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_event'));
    }
    
    function index(){
        $data['record']=  $this->mod_event->tampilkan_data();
        $this->template->load('adm/template', 'adm/event/event',$data);
    }
    
    function hapus(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('event','id_event',$id);
        redirect('adm/event');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/event/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_event->baru($data['file_name']);
             if($data['file_name']!=''){
             UploadImage($data['file_name']);}
            redirect('adm/event');
        }else{
            $this->template->load('adm/template', 'adm/event/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/event/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_event->update($data['file_name']);
                     if($data['file_name']!=''){
             UploadImage($data['file_name']);}
            redirect('adm/event');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('event','id_event',$id)->row_array();
            $this->template->load('adm/template', 'adm/event/edit',$data);
        }   
    }
}