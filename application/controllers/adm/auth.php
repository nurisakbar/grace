<?php
class auth extends CI_Controller{
    
    
    function __construct() {
        parent::__construct();
        $this->load->model(array('mod_users'));
    }
    
    function index(){
        redirect('adm/auth/login');
    }
    
    function login(){
        if(isset($_POST['submit'])){
            $chekLogin=  $this->mod_users->chek_login();
            if($chekLogin==0){
                redirect('adm/auth/login');
            }else{
                $this->session->set_userdata($chekLogin);
                redirect('adm/publik/dashboard');
            }
        }else{
            $this->load->view('adm/login');            
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect('adm/auth/login');
    }
    
    
}