<?php
class berita extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_berita'));
    }
    
    function index(){
        $data['record']=  $this->mod_berita->tampilkan_data();
        $this->template->load('adm/template', 'adm/berita/berita',$data);
    }
    
    function hapus(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('berita','id_berita',$id);
        redirect('adm/berita');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/berita/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_berita->baru($data['file_name']);
             if($data['file_name']!=''){
             UploadImage($data['file_name']);}
            redirect('adm/berita');
        }else{
            $this->template->load('adm/template', 'adm/berita/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/berita/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_berita->update($data['file_name']);
                     if($data['file_name']!=''){
             UploadImage($data['file_name']);}
            redirect('adm/berita');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('berita','id_berita',$id)->row_array();
            $this->template->load('adm/template', 'adm/berita/edit',$data);
        }   
    }
}