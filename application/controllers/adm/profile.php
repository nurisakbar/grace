<?php
class profile extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_profile'));
    }
    
    function index(){
        $data['record']=  $this->mod_profile->tampilkan_data();
        $this->template->load('adm/template', 'adm/profile/profile',$data);
    }
    
    function delete(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('profile','id_profile',$id);
        redirect('adm/profile');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/profil/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_profile->baru($data['file_name']);
            redirect('adm/profile');
        }else{
            $this->template->load('adm/template', 'adm/profile/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/profil/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_profile->update($data['file_name']);
            redirect('adm/profile');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('profile','id_profile',$id)->row_array();
            $this->template->load('adm/template', 'adm/profile/edit',$data);
        }   
    }
}