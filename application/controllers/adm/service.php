<?php
class service extends ci_controller{
    
    function __construct() {
        parent::__construct();
        chek_login();
        $this->load->model(array('mod_service'));
    }
    
    function index(){
        $data['record']=  $this->mod_service->tampilkan_data();
        $this->template->load('adm/template', 'adm/service/service',$data);
    }
    
    function hapus(){
        $id=  $this->uri->segment(4);
        $this->mod_umum->delete('service','id_service',$id);
        redirect('adm/service');
    }
    
    function baru(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/service/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
      
            $this->mod_service->baru($data['file_name']);
             if($data['file_name']!=''){
             UploadImage($data['file_name'],'service');}
            redirect('adm/service');
        }else{
            $this->template->load('adm/template', 'adm/service/manage');
        }   
    }
    
    
    function edit(){
        if(isset($_POST['submit'])){
            $config['upload_path'] = './assets/service/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '10000';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $data=$this->upload->data();
            $this->mod_service->update($data['file_name']);
                     if($data['file_name']!=''){
             UploadImage($data['file_name']);}
            redirect('adm/service');
        }else{
            $id=  $this->uri->segment(4);
            $data['r']=  $this->mod_umum->get_one('service','id_service',$id)->row_array();
            $this->template->load('adm/template', 'adm/service/edit',$data);
        }   
    }
}