-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 25, 2014 at 05:41 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `grace`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `namaLengkap` varchar(40) DEFAULT '-',
  `level` enum('Super Admin','Administrator','user') DEFAULT 'user',
  `password` varchar(200) DEFAULT NULL,
  `status` enum('Aktif','Blokir') NOT NULL DEFAULT 'Aktif'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `namaLengkap`, `level`, `password`, `status`) VALUES
(1, 'admin', 'admin', 'Super Admin', 'bd8be35fbde31a9c7eb4149cd404a6a5', 'Aktif'),
(5, 'admin', 'margettya', 'Super Admin', '21232f297a57a5a743894a0e4a801fc3', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
`id_banner` tinyint(3) NOT NULL,
  `title` varchar(250) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(250) NOT NULL,
  `link` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id_banner`, `title`, `deskripsi`, `foto`, `link`) VALUES
(5, 'banner 1', 'Ini adalah contoh content untuk menjelaskan gambar banner yang sedang tampil.', 'banner1.jpg', 'www.google.com'),
(6, 'banner 2', 'Ini adalah contoh content untuk menjelaskan gambar banner yang sedang tampil. ', 'banner2.jpg', 'www.google.com'),
(7, 'banner 3', ' Ini adalah contoh content untuk menjelaskan gambar banner yang sedang tampil.', 'banner4.jpg', 'www.google.com');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`id_berita` int(5) NOT NULL,
  `hari` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(250) NOT NULL,
  `judul_eng` varchar(200) NOT NULL,
  `judul_seo` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `isi_eng` text NOT NULL,
  `foto` varchar(100) NOT NULL,
  `views` int(5) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `hari`, `tanggal`, `judul`, `judul_eng`, `judul_seo`, `isi`, `isi_eng`, `foto`, `views`) VALUES
(45, 'Monday', '2014-11-17', 'Tidak seperti anggapan banyak orang', 'Eng Tidak seperti anggapan banyak orang', 'tidak-seperti-anggapan-banyak-orang', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32. Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32. Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', 'berita1.jpg', 0),
(46, 'Monday', '2014-11-17', 'Lorem Ipsum is not simply random text', 'sample Lorem Ipsum is not simply random text', 'lorem-ipsum-is-not-simply-random-text', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32. Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32. Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', 'berita2.jpg', 0),
(47, 'Monday', '2014-11-17', 'Lorem Ipsum is not simply random text', 'sample Lorem Ipsum is not simply random text', 'lorem-ipsum-is-not-simply-random-text', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32. Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32. Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', 'berita3.jpg', 0),
(35, 'Saturday', '2014-11-15', 'Hari Ke-13 Porprov, Balikpapan Koleksi 44 emas, 61 perak, 83 perunggu ', '', 'hari-ke13-porprov-balikpapan-koleksi-44-emas-61-perak-83-perunggu-', '<p>Hingga hari ke - 13 penyelenggaraan Porprov V Kaltim tahun 2014 di Samarinda, kontingen Balikpapan telah mengumpulkan 187 medali. Sampai pukul&nbsp; 22.00 wita Rabu malam (12/11), Balikpapan mengumpulkan&nbsp; 43 emas, 61 perak dan 83 perunggu. Posisi Balikpapan berada&nbsp; di bawah tuan rumah Samarinda dan Kutai Kartanegara yang berada di peringkat pertama dan kedua.<br /><br /><br />Menurut Kabid Pemuda dan Olahraga Disporabudpar Balikpapan Irfan Taufik, perolehan medali bisa bertambah, mengingat ada beberapa cabang olahraga unggulan yang berpeluang meraih medali namun belum dipertandingkan. Cabor unggulan tersebut diantaranya Karate, Pencak Silat,&nbsp; Muaithai, Tarung Drajat, Voly putra&nbsp; serta Wushu.<br />lebih lanjut Ia berharap medali emas bisa menyentuh angka 100,&nbsp; namun kondisi tersebut akan dikembalikan ke masing-masing cabor dalam menghadapi pertandingan.<br /><br />"Dari cabang Karate kita harapkan bisa mendulang emas yang lumayan, untuk cabang Muathai kita punya atlet nasional, termasuk voly putra," ujar Irfan. Pada Porprov empat tahun silam di Bontang, Balikpapan menjadi juara dua namun pada Porprov kali ini Balikpapan masih berada diperingkat III.<br /><br />"Perolehan medali tidak menjadi&nbsp; tolak ukur mengingat banyaknya atlet nasional yang ikut di porprov V ini," tegasnya. "Kita&nbsp; benar-benar menggunakan atlet daerah yang merupakan atlet binaan sendiri. Jadi, perolehan medali tidak menjadi tolak ukur tapi kita melihat bagaimana meningkatnya kompetensi diri atlet dari pelaksanaan porprov ini," terangnya. Ia berharap agar ajang olahraga daerah ini bisa mengasah kemampuan atlet Balikpapan dan daerah lain untuk meraih pestasi yang lebih baik lagi.(HMS/Tim Liputan)</p>', '<p>Hingga hari ke - 13 penyelenggaraan Porprov V Kaltim tahun 2014 di Samarinda, kontingen Balikpapan telah mengumpulkan 187 medali. Sampai pukul&nbsp; 22.00 wita Rabu malam (12/11), Balikpapan mengumpulkan&nbsp; 43 emas, 61 perak dan 83 perunggu. Posisi Balikpapan berada&nbsp; di bawah tuan rumah Samarinda dan Kutai Kartanegara yang berada di peringkat pertama dan kedua.<br /><br /><br />Menurut Kabid Pemuda dan Olahraga Disporabudpar Balikpapan Irfan Taufik, perolehan medali bisa bertambah, mengingat ada beberapa cabang olahraga unggulan yang berpeluang meraih medali namun belum dipertandingkan. Cabor unggulan tersebut diantaranya Karate, Pencak Silat,&nbsp; Muaithai, Tarung Drajat, Voly putra&nbsp; serta Wushu.<br />lebih lanjut Ia berharap medali emas bisa menyentuh angka 100,&nbsp; namun kondisi tersebut akan dikembalikan ke masing-masing cabor dalam menghadapi pertandingan.<br /><br />"Dari cabang Karate kita harapkan bisa mendulang emas yang lumayan, untuk cabang Muathai kita punya atlet nasional, termasuk voly putra," ujar Irfan. Pada Porprov empat tahun silam di Bontang, Balikpapan menjadi juara dua namun pada Porprov kali ini Balikpapan masih berada diperingkat III.<br /><br />"Perolehan medali tidak menjadi&nbsp; tolak ukur mengingat banyaknya atlet nasional yang ikut di porprov V ini," tegasnya. "Kita&nbsp; benar-benar menggunakan atlet daerah yang merupakan atlet binaan sendiri. Jadi, perolehan medali tidak menjadi tolak ukur tapi kita melihat bagaimana meningkatnya kompetensi diri atlet dari pelaksanaan porprov ini," terangnya. Ia berharap agar ajang olahraga daerah ini bisa mengasah kemampuan atlet Balikpapan dan daerah lain untuk meraih pestasi yang lebih baik lagi.(HMS/Tim Liputan)</p>', 'berita4.jpg', 5),
(36, 'Saturday', '2014-11-15', 'Karate Do Balikpapan Sumbang Medali Perak dan Perunggu ', '', 'karate-do-balikpapan-sumbang-medali-perak-dan-perunggu-', '<p>Tim Karate - Do Balikpapan pada hari ke 11 Porprov harus puas dengan perolehan medali perak dan perunggu. Pada pertandingan yang baru dimulai Rabu (12/11), di Folder Air Hitam, dua&nbsp; medali ini disumbangkan Rida Anjani yang berlaga di kelas perorangan putri dan Ridho di kelas 84 kilogram. "Hari ini kita meraih dua medali tapi masih banyak pertandingan yang akan&nbsp; berlangsung besok," ujar M. Arif Sekretaris Pengurus Beras Federasi Olahraga Karate-do Indonesia (PB Forki) Balikpapan, didampingi Wakil Ketua II Forki Balikpapan Muhammad&nbsp; Arsyad.<br /><br />Menurut M. Arif, pada ajang Porprov ke V kali ini, cabang olahraga Karate-do memperebutkan 17 emas, dan Ia optimis Balikpapan dapat menambah medali karena diperkuat oleh atlet Karate do&nbsp; unggulan di sejumlah kelas. "Atlet unggulan kita di kelas 55 Kg putra adalah Titis, lalu ada Retno dikelas -50 kg putri, Heru Ferdinan dikelas -67 kg dan Astriyanti +84 kg putri," jelas M. Arif.<br /><br />Pada cabor ini ditargetkan empat medali emas namun kemungkinan akan berkurang. Menurut M. Arif hal tersebut karena disinyalir&nbsp; tuan rumah memasukan atlet nasional luar daerah asal DKI Jakarta.&nbsp; "Nama itu muncul saat technical meetings.Saat itu kita protes tapi protes kita kayak dipimpong. Tapi KONI sudah ajukan protes," ujarnya kecewa. Forki Balikpapan sangat menyayangkan adanya kejadian tersebut dan berharap tidak terulang pada pertandingan berikutnya. (HMS-tim liputan)</p>', '', 'berita6.jpg', 16),
(34, 'Saturday', '2014-11-15', 'Tenis Tunggal Putra Persembahkam Emas ', '', 'tenis-tunggal-putra-persembahkam-emas-', '<p>Samarinda, Humaspro - Hingga hari ke - 13 penyelenggaraan Porprov V Kaltim tahun 2014 di Samarinda, kontingen Balikpapan telah mengumpulkan 187 medali.&nbsp;Sampai pukul&nbsp; 22.00 wita Rabu malam (12/11), Balikpapan mengumpulkan&nbsp; 43 emas, 61 perak dan 83 perunggu.&nbsp;Posisi Balikpapan berada &nbsp;di bawah tuan rumah Samarinda dan Kutai Kartanegara yang berada di peringkat pertama dan kedua.</p>\n<p style="text-align: justify;"><br />Menurut <span style="line-height: 15.8079996109009px; text-align: justify;">Kabid Pemuda dan Olahraga Disporabudpar Balikpapan Irfan Taufik, p</span>erolehan medali bisa bertambah, mengingat ada beberapa cabang olahraga unggulan yang berpeluang meraih medali namun belum dipertandingkan.&nbsp;Cabor unggulan tersebut diantaranya Karate, Pencak Silat,&nbsp; Muaithai, Tarung Drajat, Voly putra&nbsp; serta Wushu.<br />lebih lanjut Ia berharap medali emas bisa menyentuh angka 100, &nbsp;namun kondisi tersebut akan dikembalikan ke masing-masing cabor dalam menghadapi pertandingan.</p>\n<p style="text-align: justify;">"Dari cabang Karate kita harapkan bisa mendulang emas yang lumayan, untuk cabang Muathai kita punya atlet nasional,&nbsp;termasuk voly putra," ujar Irfan.&nbsp;Pada Porprov empat tahun silam di Bontang, Balikpapan menjadi juara dua namun pada Porprov kali ini Balikpapan masih berada diperingkat III.<br /> <br />"Perolehan medali tidak menjadi&nbsp; tolak ukur mengingat banyaknya atlet nasional yang ikut di porprov V ini," tegasnya.&nbsp;"Kita&nbsp; benar-benar menggunakan atlet daerah yang merupakan atlet binaan sendiri. Jadi, perolehan medali tidak menjadi tolak ukur tapi kita melihat bagaimana meningkatnya kompetensi diri atlet dari pelaksanaan porprov ini," terangnya. Ia berharap agar ajang olahraga daerah ini bisa mengasah kemampuan atlet Balikpapan dan daerah lain untuk meraih pestasi yang lebih baik lagi.(HMS/Tim Liputan)</p>', '', 'berita9.jpg', 5);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
`id_cont` int(5) NOT NULL,
  `judul` varchar(250) NOT NULL,
  `judul_eng` varchar(200) NOT NULL,
  `judul_seo` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `isi_eng` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id_cont`, `judul`, `judul_eng`, `judul_seo`, `isi`, `isi_eng`, `tanggal`) VALUES
(1, 'sejarah perusahaan', 'compny history', 'sejarah-perusahaan', '<p><span><span>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</span></span></p>\n<p><span><span>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</span></span></p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p><span><span>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</span></span></p>', '2014-12-19 09:26:33'),
(2, 'visi dan misi', 'vision and mision', 'visi-dan-misi', '<p><span><span>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</span></span></p>\n<p><span><span>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</span></span></p>', '<p><span><span>Eng Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</span></span></p>\n<p><span><span>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</span></span></p>', '2014-12-19 09:39:19'),
(3, 'profile organisai', 'profile organisation', 'profile-organisai', '<p><span><span>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</span></span></p>\n<p><span><span>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</span></span></p>', '<p><span><span>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</span></span></p>\n<p><span><span>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</span></span></p>', '2014-12-19 09:39:43'),
(4, 'HSE policy', 'hse policy', 'hse-policy', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:31:41'),
(5, 'q police', 'q police', 'q-police', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:32:34'),
(6, 'hrm police', 'hrm police', 'hrm-police', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:32:52'),
(7, 'product', 'product', 'product', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:34:34'),
(8, 'pelayanan', 'service', 'pelayanan', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:34:48'),
(9, 'pelayanan', 'service', 'pelayanan', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:35:09'),
(10, 'barang', 'product', 'barang', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-19 19:35:23'),
(11, 'ini adalah lowongan kerja', 'this is job newxs', 'ini-adalah-lowongan-kerja', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '<p>english&nbsp;</p>\n<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2014-12-22 18:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
`id_event` int(5) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(150) NOT NULL,
  `judul_seo` varchar(200) NOT NULL,
  `judul_eng` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `content_eng` text NOT NULL,
  `foto` varchar(250) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `tanggal`, `judul`, `judul_seo`, `judul_eng`, `content`, `content_eng`, `foto`) VALUES
(4, '2014-11-26', 'event 1', 'event-1', 'service title', '<p>content indo</p>', '<p>content isi</p>', 'berita3.jpg'),
(5, '2014-11-26', 'event 2', 'event-2', 'service title', '<p>content indo</p>', '<p>content isi</p>', 'berita9.jpg'),
(6, '2014-11-26', 'event 3', 'event-3', 'service title', '<p>content indo</p>', '<p>content isi</p>', 'berita8.jpg'),
(7, '2014-11-26', 'evetnt 4', 'evetnt-4', 'service title', '<p>content indo</p>', '<p>content isi</p>', 'berita4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
`id_gallery` int(3) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` varchar(250) NOT NULL,
  `deskripsi_eng` text NOT NULL,
  `title` varchar(250) NOT NULL,
  `title_eng` varchar(200) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id_gallery`, `foto`, `deskripsi`, `deskripsi_eng`, `title`, `title_eng`) VALUES
(26, 'Desert.jpg', 'sdf', '', 'sdf', ''),
(24, 'Tulips.jpg', 'dimana', '', 'bila', ''),
(27, 'Hydrangeas.jpg', 'Ini adalah foto yang diambil saat upacara hari kemerdekaan', '', 'Foto Kemerdekaan', ''),
(28, 'Chrysanthemum.JPG', 'as', '', 'bea', ''),
(31, '10409258_1493076270980960_4683298671833316235_n.jpg', '<p>isi12</p>', '<p>content12</p>', 'judul12', 'title12');

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE IF NOT EXISTS `konfigurasi` (
`id` int(5) NOT NULL,
  `title` text NOT NULL,
  `keyword` text NOT NULL,
  `deskripsi` text NOT NULL,
  `nama_comp` varchar(200) NOT NULL,
  `slogan` varchar(250) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `hape` varchar(15) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `running_teks` text NOT NULL,
  `logo` varchar(100) NOT NULL,
  `hitstats` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`id`, `title`, `keyword`, `deskripsi`, `nama_comp`, `slogan`, `alamat`, `email`, `website`, `telp`, `hape`, `fax`, `running_teks`, `logo`, `hitstats`) VALUES
(1, '1Selamat Datang di Website - Dinas Pendidikan Kota Balikpapan', '1Dinas Pendidikan Kota Balikpapan', '1Dinas Pendidikan Kota Balikpapan', '1Dinas Kebersihan, Pertamanan', 'Kubangun Kujaga Kubela', '1Jalan Ruhui Rahayu I  Kelurahan Sepinggan', '1saya@balikpapan.go.id', '', '10542-7171371, 0543300', '123', '10542-900001', 'WEBSITE RESMI BADAn LingKunGan HidUp', 'logo.png', 'saya adalah');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE IF NOT EXISTS `kontak` (
`id_kontak` int(11) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telpon` varchar(12) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id_kontak`, `nama`, `email`, `telpon`, `pesan`, `tanggal`) VALUES
(1, 'fdfd', 'nuris.akbar@gmail.com', '0454545', 'fdfdfdfdfdf', '2014-12-19 20:31:04'),
(5, 'fdfd', 'nuris.akbar@gmail.com', '0454545', 'fdfdfdfdfdf', '2014-12-19 20:45:43');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`id_menu` tinyint(3) unsigned NOT NULL,
  `id_content` int(11) NOT NULL,
  `parent_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `title_eng` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL DEFAULT '',
  `menu_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `jenis` enum('modulle','teks','pdf','url') NOT NULL DEFAULT 'teks',
  `status_front` enum('Aktif','Non Aktif') NOT NULL DEFAULT 'Non Aktif',
  `status_admin` enum('Aktif','Non Aktif') NOT NULL DEFAULT 'Non Aktif'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `id_content`, `parent_id`, `title`, `title_eng`, `url`, `menu_order`, `jenis`, `status_front`, `status_admin`) VALUES
(1, 0, 0, 'profil', 'Profile', '#', 0, 'teks', 'Non Aktif', 'Non Aktif'),
(3, 0, 0, 'Bisnis Kami', 'Our Bisniss', '#', 0, 'teks', 'Non Aktif', 'Non Aktif'),
(4, 0, 0, 'Berita', 'News', 'news/index/', 0, 'teks', 'Non Aktif', 'Non Aktif'),
(5, 0, 0, 'Karir', 'Career', 'p/pages/11/ini-adalah-lowongan-kerja', 0, 'teks', 'Non Aktif', 'Non Aktif'),
(6, 0, 0, 'Kontak Kami', 'Contac Us', 'kontak', 0, 'teks', 'Non Aktif', 'Non Aktif'),
(0, 0, 0, 'Beranda', 'Home', 'p', 0, 'teks', 'Non Aktif', 'Non Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
`id_profile` int(11) NOT NULL,
  `tahun` varchar(100) NOT NULL,
  `gambar` text NOT NULL,
  `content` text NOT NULL,
  `content_eng` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id_profile`, `tahun`, `gambar`, `content`, `content_eng`) VALUES
(2, '1998', '4.png', '<p>indo&nbsp;<span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>', '<p><span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>'),
(3, '1993', '2.png', '<p>versi indo&nbsp;<span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>', '<p><span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>'),
(4, '1994', '3.png', '<p>versi indo&nbsp;<span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>', '<p><span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>'),
(5, '1995', '7.png', '<p>versi indo&nbsp;Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</p>', '<p><span>Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</span></p>'),
(7, 'dsdsd', 'berita91.jpg', '<p>sdsdsdsd</p>', '<p>dsdsd</p>');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
`id_service` int(5) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `judul_eng` varchar(200) NOT NULL,
  `judul_seo` text NOT NULL,
  `content` text NOT NULL,
  `content_eng` text NOT NULL,
  `foto` varchar(250) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id_service`, `judul`, `judul_eng`, `judul_seo`, `content`, `content_eng`, `foto`) VALUES
(1, 'contoh service', 'sample service', 'contoh-service', '<div>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</div>', '<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>', 'small_44.jpg'),
(2, 'contoh service 2', 'sample service 2', 'contoh-service-2', '<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>', '<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>', 'small_11.jpg'),
(3, 'contoh service 3', 'sample service 3', 'contoh-service-3', '<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>', '<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;lora ipsum&nbsp;</p>', 'medium_6.jpg'),
(4, 'contoh service 4', 'sample service 4', 'contoh-service-4', '<p>lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;</p>', '<p>lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;lora ipsum &nbsp;</p>', 'medium_2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE IF NOT EXISTS `submenu` (
`id_submenu` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_eng` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL,
  `id_menu` int(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`id_submenu`, `title`, `title_eng`, `url`, `id_menu`) VALUES
(1, 'Sejarah Perusahaan', 'company history', 'profile', 1),
(2, 'membangun komitment', 'build commitment', '#', 1),
(3, 'visi dan misi', 'vision and mission', 'p/pages/2/visi-dan-misi', 1),
(4, 'profil organisasi', 'organization profile', 'p/pages/3/profile-organisai', 1),
(5, 'pertambangan', 'mining', '#', 3),
(6, 'minyak dan gas', 'oil and gas', '#', 3),
(7, 'peralatan kami', 'our equipment', '#', 3);

-- --------------------------------------------------------

--
-- Table structure for table `subsubmenu`
--

CREATE TABLE IF NOT EXISTS `subsubmenu` (
`id_subsubmenu` int(11) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `judul_eng` varchar(150) NOT NULL,
  `link` varchar(100) NOT NULL,
  `id_submenu` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `subsubmenu`
--

INSERT INTO `subsubmenu` (`id_subsubmenu`, `judul`, `judul_eng`, `link`, `id_submenu`) VALUES
(1, 'HSE Police', 'HSE Policy', 'p/pages/4/hse-policy', 2),
(2, 'Q Police', 'Q Police', 'p/pages/5/q-police', 2),
(3, 'HRM Police', 'HRM POlice', 'p/pages/6/hrm-police', 2),
(4, 'Produk', 'Product', 'p/pages/7/product', 5),
(5, 'Service', 'Service', 'p/pages/8/pelayanan', 5),
(6, 'Produk', 'Product', 'p/pages/10/barang', 6),
(7, 'Service', 'Service', 'p/pages/9/pelayanan', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
 ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
 ADD PRIMARY KEY (`id_cont`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
 ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
 ADD PRIMARY KEY (`id_kontak`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
 ADD PRIMARY KEY (`id_profile`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
 ADD PRIMARY KEY (`id_service`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
 ADD PRIMARY KEY (`id_submenu`);

--
-- Indexes for table `subsubmenu`
--
ALTER TABLE `subsubmenu`
 ADD PRIMARY KEY (`id_subsubmenu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
MODIFY `id_banner` tinyint(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `id_berita` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
MODIFY `id_cont` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
MODIFY `id_event` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
MODIFY `id_gallery` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `id_menu` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
MODIFY `id_profile` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
MODIFY `id_service` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
MODIFY `id_submenu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subsubmenu`
--
ALTER TABLE `subsubmenu`
MODIFY `id_subsubmenu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
