
<link href="css/begog.css" rel="stylesheet" type="text/css">
			
<div class="box">
<div class="title-box">Our Businesses</div>

<div class="list-box">

			<script type="text/javascript">
			$( document ).ready(function() {
			  
			   	$("#over-1").hover(
				   function() {
				     $("#capt-1").fadeIn(100);
				  	 $("#over-1 h1").hide();
				   },
				   function() {
				      $("#capt-1").fadeOut(100);
				  	  $("#over-1 h1").show();
				   }
				);

				$("#over-2").hover(
				   function() {
				     $("#capt-2").fadeIn(100);
				  	 $("#over-2 h1").hide();
				   },
				   function() {
				      $("#capt-2").fadeOut(100);
				  	  $("#over-2 h1").show();
				   }
				);

				$("#over-3").hover(
				   function() {
				     $("#capt-3").fadeIn(100);
				  	 $("#over-3 h1").hide();
				   },
				   function() {
				      $("#capt-3").fadeOut(100);
				  	  $("#over-3 h1").show();
				   }
				);

				$("#over-4").hover(
				   function() {
				     $("#capt-4").fadeIn(100);
				  	 $("#over-4 h1").hide();
				   },
				   function() {
				      $("#capt-4").fadeOut(100);
				  	  $("#over-4 h1").show();
				   }
				);

				$("#over-5").hover(
				   function() {
				     $("#capt-5").fadeIn(100);
				  	 $("#over-5 h1").hide();
				   },
				   function() {
				      $("#capt-5").fadeOut(100);
				  	  $("#over-5 h1").show();
				   }
				);

				$("#over-6").hover(
				   function() {
				     $("#capt-6").fadeIn(100);
				  	 $("#over-6 h1").hide();
				   },
				   function() {
				      $("#capt-6").fadeOut(100);
				  	  $("#over-6 h1").show();
				   }
				);

			

			});
</script>
			
			
			<div class="the-box-over" id="over-1">
				<h1>Agribusiness</h1>
				<img alt="buah_400x400.jpg" src="images/f9f0379b8133fd82b5ab7b527e775684.jpg" />
				<a href="business-landing-pages/agribusiness">
                                
					<div class="caption-over" id="capt-1">
					<h2>Agribusiness</h2>
					<p>
						The demand for food will increase significantly as more consumers in emerging economies enter the middle class.&nbsp;					</p>
				</div>
				</a>			</div>
			
			
	  <div class="the-box-over" id="over-2">
				<h1>Insurance</h1>
				<img alt="payung_400x400.jpg" src="images/b3027c7e4c0e0f1db9645329091d80ee.jpg" />
								<a href="business-landing-pages/insurance">
								<div class="caption-over" id="capt-2">
					<h2>Insurance</h2>
					<p>
						As the economy continues to develop, Indonesians will increasingly use insurance for their wealth protection, health coverage and investment needs.&nbsp;					</p>
				</div>
				</a>			</div>
			
			
	  <div class="the-box-over" id="over-3">
				<h1>Property</h1>
				<img alt="gedung_400x400.jpg" src="images/fe0bd488b694acf6c081fe0134bf4ea7.jpg" />
								<a href="business-landing-pages/property">
								<div class="caption-over" id="capt-3">
					<h2>Property</h2>
					<p>
						Indonesia&#39;s property market is resilient as demand for quality properties of all types is increasing, incomes are growing and available land in Jakarta is limited.&nbsp;					</p>
				</div>
				</a>			</div>
			
			
	  <div class="the-box-over" id="over-4">
				<h1>Consumer Goods</h1>
				<img alt="belanja_400x400.jpg" src="images/143ff63f656b08e138d0b4aa72ebe640.jpg" />
								<a href="business-landing-pages/consumer-goods">
								<div class="caption-over" id="capt-4">
					<h2>Consumer Goods</h2>
					<p>
						Indonesia&#39;s consumer class is projected to triple by 2030.&nbsp;					</p>
				</div>
				</a>
                </div>
			
			
	  
			

  </div>	
		<!-- list-box end -->

	</div>
	<!-- box end -->