<!-- jQuery library (served from Google) -->
 <script src="<?php echo base_url();?>assets/js/jquery-1.11.0.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="<?php echo base_url();?>assets/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="<?php echo base_url();?>assets/jquery.bxslider.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/custom.css" rel="stylesheet" type="text/css">

	<div class="sliderContainer fullWidth clearfix">
		<div id="full-width-slider" class="">
			<ul class="bxslider">

                    
                    <?php
                    $banner=$this->db->get('banner')->result();
                    foreach ($banner as $b){
                    ?>
                    
                    <li>
				    <a href="#">				    
                    <img alt="sequistower-1632x500.jpg" src="<?php echo base_url().'assets/banner/'.$b->foto;?>" />
					</a><div class="infoBlock infoBlockLeftBlack rsABlock">
					  <h4><?php echo $b->title?></h4>
					  <p><?php echo $b->deskripsi?></p>
				    </div> 
                    </li>
                    <?php
                    }
                    ?>
                    
                    
			</ul>
		</div>
	 </div>

<!-- banner end -->
<script>
	jQuery(document).ready(function($) {
	  $('.bxslider').bxSlider({
	  	adaptiveHeight: true, 
		auto: true,
	  });
	});
</script>